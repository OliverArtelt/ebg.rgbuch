﻿using System;
using System.Data.Objects;
using System.Linq.Expressions;


namespace Rechnungsbuch
{

    public static class ObjectQueryExtended
    {

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Source: http://msmvps.com/blogs/matthieu/archive/2008/06/06/entityframework-include-with-func.aspx
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="qry"></param>
        /// <param name="subSelector"></param>
        /// <returns></returns>
        public static ObjectQuery<T> Include<T>(this ObjectQuery<T> qry, Expression<Func<T, object>> subSelector)
        {

            return qry.Include(((subSelector.Body as MemberExpression).Member as System.Reflection.PropertyInfo).Name);
        }
    }
}