﻿using System;
using Rechnungsbuch.Entities;
using System.Collections.Generic;


namespace Rechnungsbuch.Repositories
{
    
    public interface IMandantRepository
    {
        
        IList<Mandant> MandantenMitKontenplan { get; }
    }
}
