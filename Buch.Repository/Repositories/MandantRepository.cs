﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Rechnungsbuch.Entities;
using Rechnungsbuch.Storage;


namespace Rechnungsbuch.Repositories
{
   
    /// <summary>
    /// alle Mandanten mit Buchungsjahren zum Filtern geben
    /// </summary>
    [Export(typeof(IMandantRepository))]
    public class MandantRepository : IMandantRepository
    {

        private List<Mandant> mandanten;

        [Import(typeof(IDbFactory))]
        public Lazy<IDbFactory> DbFactory { get; set; }


        public IList<Mandant> MandantenMitKontenplan 
        {

            get {

            System.Diagnostics.Debug.WriteLine("> MandantRepository.MandantenMitKontenplan");
            

                if (mandanten != null) return mandanten;

                using (var ctx = DbFactory.Value.CreateBuchContext()) {

                    mandanten = ctx.Mandanten.Include("Buchungsjahre")
                                             .Include("Kontenklassen.Kontentyp")
                                             .ToList();

            System.Diagnostics.Debug.WriteLine("< MandantRepository.MandantenMitKontenplan");
            
                    return mandanten;
                }
            }
        }
    }
}
