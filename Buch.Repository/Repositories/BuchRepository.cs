﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Objects;
using System.Linq;
using Rechnungsbuch.Entities;
using Rechnungsbuch.Storage;
using Rechnungsbuch.Users;


namespace Rechnungsbuch.Repositories
{
    
    [Export(typeof(IBuchRepository)), PartCreationPolicy(CreationPolicy.NonShared)]
    public class BuchRepository : IBuchRepository, IDisposable
    {

        private int[]        modifiedBy;
        private BuchEntities context; 
        private Buchungsjahr jahr;

        private EntityObservableCollection<Konto>   konten;
        private EntityObservableCollection<Rkz>     rkzSet;
        private EntityObservableCollection<Buchung> buchungen;


        [Import(typeof(ICurrentOsUser))]
        public Lazy<ICurrentOsUser> User { get; set; }

        
        [ImportingConstructor]
        public BuchRepository(IDbFactory factory)
        {

            context = factory.CreateBuchContext();
        }


#region P E S S I M I S T I C   L O C K

        public BuchLockKey AcquireLock(Buchung bg)
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.AcquireLock");


	        //@Id int,
	        //@Owner nvarchar(120),
	        //@Station nvarchar(120) = NULL,
	        //@IsSuccessful bit OUTPUT,
	        //@OldOwner nvarchar(120) OUTPUT,
	        //@OldStation nvarchar(120) OUTPUT,
	        //@OldDate datetime OUTPUT
            var parIsSuccessful = new ObjectParameter("IsSuccessful", typeof(bool));
            var parOldOwner     = new ObjectParameter("OldOwner",     typeof(String));
            var parOldStation   = new ObjectParameter("OldStation",   typeof(String));
            var parOldDate      = new ObjectParameter("OldDate",      typeof(DateTime));

            context.AcquireRebuchLock(bg.IDBuchung, User.Value.SystemName, System.Environment.MachineName, 
                                      parIsSuccessful, parOldOwner, parOldStation, parOldDate);

            System.Diagnostics.Debug.WriteLine("< BuchRepository.AcquireLock");

            return new BuchLockKey { Buchung = bg, IsSuccessful = (bool)parIsSuccessful.Value, Benutzer = (String)parOldOwner.Value,
                                     Station = (String)parOldStation.Value, Sperrzeit = (DateTime)parOldDate.Value }; 
        }


        public void ReleaseLock(BuchLockKey key)
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.ReleaseLock");


            context.ReleaseRebuchLock(key.Buchung.IDBuchung);    

            System.Diagnostics.Debug.WriteLine("< BuchRepository.ReleaseLock");
        }


#endregion


#region A D D I N G


        public Rkz NeueRkz()
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.NeueRkz");
            
            if (disposed) throw new ObjectDisposedException("Interner Fehler: ungültiger Objektkontext.");

            var rkz = new Rkz { Name = "Neue RKZ/FBZ" };
            ((EntityObservableCollection<Rkz>)RkzSet).Add(rkz);

            context.SaveChanges();
            context.Refresh(RefreshMode.StoreWins, rkz);

            System.Diagnostics.Debug.WriteLine("< BuchRepository.NeueRkz");
            

            return rkz; 
        }
            
        /// <summary>
        /// neues Konto mit nächster freien Nummer reservieren
        /// </summary>
        /// <remarks>
        /// Stored proc ermittelt nächste Nummer
        /// </remarks>
        public Konto NeuesKonto(Kontotyp typ)
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.NeuesKonto");
            
             
            if (disposed) throw new ObjectDisposedException("Interner Fehler: ungültiger Objektkontext.");
            
            String ktName = String.Empty;

            switch (typ) {

                case Kontotyp.Debitor:
                
                    ktName = "Neues Debitorenkonto";
                    break;

                case Kontotyp.Kreditor:
                
                    ktName = "Neues Kreditorenkonto";
                    break;

                case Kontotyp.Sonstige:
                
                    ktName = "Neues sonstiges Konto";
                    break;

                default:

                    throw new ApplicationException("Interner Fehler: Kontentyp nicht unterstützt.");
            }

        
            var kto = new Konto { KeyTyp = (int)typ, Name = ktName, KeyMandant = jahr.KeyMandant };
            ((EntityObservableCollection<Konto>)Konten).Add(kto);
            context.SaveChanges();
            context.Refresh(RefreshMode.StoreWins, kto);

            System.Diagnostics.Debug.WriteLine("< BuchRepository.NeuesKonto");
            

            return kto;
        }
          

        /// <summary>
        /// nächste freie Buchung geben
        /// </summary>
        /// <remarks>
        /// Stored proc ermittelt nächste Nummer
        /// </remarks>
        public Buchung NeueBuchung()
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.NeueBuchung");
            
            if (disposed) throw new ObjectDisposedException("Interner Fehler: ungültiger Objektkontext.");
            
            var bg = new Buchung { Eingang = DateTime.Today, Buchungsjahr = jahr };
            ((EntityObservableCollection<Buchung>)Buchungen).Add(bg);
            context.SaveChanges();

            context.Refresh(RefreshMode.StoreWins, bg);

            System.Diagnostics.Debug.WriteLine("< BuchRepository.NeueBuchung");
            
            return bg; 
        }

#endregion


        /// <summary>
        /// Mandant mit Buchungen etc geben
        /// </summary>
        /// <param name="mandantJahr">Buchungsjahr als Filterobjekt (ist detached)</param>
        /// <returns>Mandant mit Buchungen des Jahres, Kontenplan und Rkz</returns>
        public void LoadMandant(Buchungsjahr jahr)
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.LoadMandant");
            

            this.jahr = context.Buchungsjahre.Include(p => p.Mandant).First(p => p.IdBuchungsjahr == jahr.IdBuchungsjahr);

            var kqry = context.Konten.Where(p => p.KeyMandant == jahr.Mandant.IdMandant).OrderBy(p => p.KontoNr);
            konten = new EntityObservableCollection<Konto>(context, "Konten", kqry);

            var rqry = context.RkzSet.OrderBy(p => p.Name);
            rkzSet = new EntityObservableCollection<Rkz>(context, "RkzSet", rqry);

            var bqry = context.Buchungen.Where(p => p.KeyBuchungsjahr == jahr.IdBuchungsjahr).OrderBy(p => p.BelegNr);
            buchungen = new EntityObservableCollection<Buchung>(context, "Buchungen", bqry);

            System.Diagnostics.Debug.WriteLine("< BuchRepository.LoadMandant");
            
        }


        public ICollection<Konto> Konten { get { return konten; } }

        public ICollection<Rkz> RkzSet { get { return rkzSet; } }

        public ICollection<Buchung> Buchungen { get { return buchungen; } }


        public int[] ModifiedBy
        {
            
            get {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.ModifiedBy");
            
            
                if (disposed) throw new ObjectDisposedException("Interner Fehler: ungültiger Objektkontext.");

                if (modifiedBy == null) modifiedBy = context.BuchungHistorySet
                                                            .Where(p => p.Benutzer == User.Value.SystemName)
                                                            .Select(p => p.KeyRebuch)
                                                            .Distinct()
                                                            .OrderBy(p => p)
                                                            .ToArray();

            System.Diagnostics.Debug.WriteLine("< BuchRepository.ModifiedBy");
            
                return modifiedBy;
            }
        }


        public void Save()
        {

            System.Diagnostics.Debug.WriteLine("> BuchRepository.Save");
            
            
            if (disposed) throw new ObjectDisposedException("Interner Fehler: ungültiger Objektkontext.");

            context.SaveChanges();
            //jetzt geänderte Buchungen nachtragen forcieren
            modifiedBy = null;

            System.Diagnostics.Debug.WriteLine("< BuchRepository.Save");
            
        }


#region D I S P O S I N G

        
        private bool disposed = false;


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {

            if (!this.disposed) {

                if (disposing) {

                    context.Dispose();
                }

                disposed = true;
            }
        }

        ~BuchRepository()
        {
            Dispose(false);
        }
        
#endregion
  
    }
}
