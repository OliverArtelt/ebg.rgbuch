﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rechnungsbuch.Entities;

namespace Rechnungsbuch.Repositories
{
    
    public class BuchLockKey
    {

        /// <summary>
        /// gesperrter Buchungssatz
        /// </summary>
        public Buchung Buchung { get; set; }
        /// <summary>
        /// konnte Sperre erfolgreich angefordert werden?
        /// </summary>
        public bool IsSuccessful { get; set; }
        /// <summary>
        /// Benutzer der diese Sperre angefordert hat (wenn erfolgreich, dann anfordernder Benutzer)
        /// </summary>
        public String Benutzer { get; set; }
        /// <summary>
        /// Workstation von der diese Sperre angefordert wurde (wenn erfolgreich, dann Workstation des anfordernden Benutzers)
        /// </summary>
        public String Station { get; set; }
        /// <summary>
        /// Zeit wann diese Sperre angefordert wurde
        /// </summary>
        public DateTime Sperrzeit { get; set; }
    }
}
