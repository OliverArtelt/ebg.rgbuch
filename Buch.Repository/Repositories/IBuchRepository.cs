﻿using System;
using Rechnungsbuch.Entities;
using System.Collections.Generic;


namespace Rechnungsbuch.Repositories
{
    
    public interface IBuchRepository : IDisposable
    {

        void Save();

        /// <summary>
        /// Buchungssätze werden bei der Bearbeitung pessimistisch gesperrt
        /// </summary>
        /// <param name="bg">zu bearbeitende Buchung</param>
        /// <returns>Sperre</returns>
        BuchLockKey AcquireLock(Buchung bg);
        void ReleaseLock(BuchLockKey key);
        /// <summary>
        /// Buchungsjahr des Mandanten laden
        /// </summary>
        /// <param name="jahr"></param>
        void LoadMandant(Buchungsjahr jahr);


        Rkz NeueRkz();
        Konto NeuesKonto(Kontotyp typ);
        Buchung NeueBuchung();

        ICollection<Konto> Konten { get; }
        ICollection<Rkz> RkzSet { get; }
        ICollection<Buchung> Buchungen { get; }
        /// <summary>
        /// Sortierte Buchungs-Id's die der aktuelle Benutzer angefaßt hat
        /// </summary>
        int[] ModifiedBy { get; }
    }
}
