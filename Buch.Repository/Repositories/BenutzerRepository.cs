﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Rechnungsbuch.Entities;
using Rechnungsbuch.Storage;
using Rechnungsbuch.Users;


namespace Rechnungsbuch.Repositories
{

    [Export(typeof(IBenutzerRepository))]
    public class BenutzerRepository : IBenutzerRepository
    {
    
        private User current;

        [Import]
        public ICurrentOsUser CurrentOsUser { get; set; }
        [Import(typeof(IDbFactory))]
        public Lazy<IDbFactory> DbFactory { get; set; }
    

        public User Current
        {

            get {

                
                if (current != null) return current;

                
                using (var ctx = DbFactory.Value.CreateBuchContext()) {

                    var user = ctx.Users.Include(p => p.Roles)
                                  .FirstOrDefault(p => p.Username.ToLower().Contains("administrator"));

                    if (user != null) current = user;
                    else              current = new User { Name = "Gast" };


                    return current;
                }
            }
        }
    }
}
