﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Objects;
using System.Text;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Repository.Services
{
    
    [Export(typeof(IUserFriendlyMessage))]
    public class UserFriendlyMessage : IUserFriendlyMessage
    {
            
        public virtual string Explain(Exception ex)
        {

            if (ex is OptimisticConcurrencyException) {

                StringBuilder bld = new StringBuilder();
                bld.AppendLine("Die Datenbank kann nicht aktualisiert werden."); 
                bld.AppendLine("Die von Ihnen bearbeiteten Daten sind inzwischen von einem anderen Bearbeiter modifiziert worden. Bitte tragen Sie Ihre Änderungen erneut ein.");

                ExplainEntities(((UpdateException)ex).StateEntries, bld);
                return bld.ToString();
            }
            
            if (ex is UpdateException) {

                StringBuilder bld = new StringBuilder();
                bld.AppendLine("Die Datenbank kann nicht aktualisiert werden."); 
                                
                if (ex.InnerException != null) {

                    if (ex.InnerException.Message.Contains("fgnBuchungHistoryRkz"))
                        bld.AppendLine("Das RKZ/FBZ darf nicht entfernt werden da es bereits bebucht wurde.");

                    if (ex.InnerException.Message.Contains("fgnBuchungHistoryKonto"))
                        bld.AppendLine("Das Konto darf nicht entfernt werden da es bereits bebucht wurde.");

                    if (ex.InnerException.Message.Contains("FK_tblKonto_tblRebuch"))
                        bld.AppendLine("Es muß ein Konto angegeben werden.");
                        
                    if (ex.InnerException.Message.Contains("FK_tblRKZ_tblRebuch"))
                        bld.AppendLine("Es muß ein RKZ/FBZ angegeben werden.");
                        
                    if (ex.InnerException.Message.Contains("chkKontoName"))   
                        bld.AppendLine("Der Name des Kontos darf nicht leer sein.");
                        
                    if (ex.InnerException.Message.Contains("chkRkzName"))   
                        bld.AppendLine("Der Name eines RKZ/FKZ darf nicht leer sein.");

                    if (ex.InnerException.Message.Contains("Timeout abgelaufen"))
                        bld.AppendLine("Die Ressource ist zur Zeit blockiert. Möglicherweise versucht ein Mitarbeiter ein Konto oder eine Buchung mit derselben Nummer anzulegen. Wiederholen Sie den Vorgang.");
                
                    if (ex.InnerException.Message.Contains("'IDX_ktKonto'"))
                        bld.AppendLine("Es existiert bereits ein Konto mit dieser Nummer. Verwenden Sie eine andere Nummer bzw. lassen Sie diese von der Anwendung vergeben.");
                
                    if (ex.InnerException.Message.Contains("Optimistic concurrency exception"))
                        bld.AppendLine("Die von Ihnen bearbeiteten Daten sind inzwischen von einem anderen Bearbeiter modifiziert worden. Bitte tragen Sie Ihre Änderungen erneut ein.");
                
                    if (ex.InnerException.Message.Contains("SqlDateTime-Überlauf"))
                        bld.AppendLine("Geben Sie bitte ein sinnvolles Datum an.");
                }

                ExplainEntities(((UpdateException)ex).StateEntries, bld);
                return bld.ToString();
            }

            if (ex is InvalidOperationException) {

                if (ex.Message.Contains("The relationship could not be changed because one or more of the foreign-key properties is non-nullable.")) {

                    return "Das Konto oder RKZ/FBZ darf nicht entfernt werden da es bereits bebucht wurde.";
                }

                if (ex.Message.Contains("eine der Fremdschlüsseleigenschaften keine NULL-Werte")) {

                    return "Das Konto oder RKZ/FBZ darf nicht entfernt werden da es bereits bebucht wurde.";
                }
            }

            return null;
        }


        private void ExplainEntities(IEnumerable<ObjectStateEntry> faulties, StringBuilder bld)
        {

            foreach (var entry in faulties) {
            
                if (entry.Entity is Buchung) bld.Append("Buchung ").AppendLine(entry.Entity.ToString());   
                if (entry.Entity is Konto)   bld.Append("Konto ").AppendLine(entry.Entity.ToString());                     
                if (entry.Entity is Rkz)     bld.Append("RKZ/FBZ ").AppendLine(entry.Entity.ToString());                     
            }
        }
    }
}
