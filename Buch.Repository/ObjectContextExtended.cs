﻿using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;


namespace Rechnungsbuch
{
    
    public static class ObjectContextExtended
    {
    
        /// <summary>
        /// Entscheiden ob geänderte Entitäten existieren
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsModified(this ObjectContext context)
        {
        
            return context.ObjectStateManager.GetObjectStateEntries(EntityState.Added).Any()    ||
                   context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified).Any() ||
                   context.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).Any();
        }
        
        /// <summary>
        /// alle Änderungen mit ClientWins speichern
        /// </summary>
        /// <remarks>
        /// http://mtaulty.com/CommunityServer/blogs/mike_taultys_blog/archive/2008/07/02/10564.aspx
        /// </remarks>
        public static int SaveChangesClientWins(this ObjectContext context)
        {
        
            bool allSaved = false;
            int count = 0;
            
            while (!allSaved) {
            
                try {
              
                    count += context.SaveChanges();
                    allSaved = true;
                }
                catch (OptimisticConcurrencyException ex) {
                
                    foreach (ObjectStateEntry item in ex.StateEntries) {
                    
                        context.Refresh(RefreshMode.ClientWins, item.Entity);
                    }
                }                
            }
            
            return count;
        }

        /// <summary>
        /// Änderungen speichern 
        /// </summary>
        /// <param name="clientWins">LastOneWins-Strategie verwenden</param>
        /// <param name="refreshAfterSave">geänderte Entities aus DB refreshen (falls DB mit Trigger rumhantiert)</param>
        /// <returns></returns>
        public static int SaveChanges(this ObjectContext context, bool clientWins, bool refreshAfterSave)
        {

            if (!refreshAfterSave) return (clientWins)? context.SaveChangesClientWins(): context.SaveChanges();

            var edits = context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified).Select(p => p.Entity);
            int i = (clientWins)? context.SaveChangesClientWins(): context.SaveChanges();
            context.Refresh(RefreshMode.StoreWins, edits);
            context.AcceptAllChanges();

            return i;
        }
        
        /// <summary>
        /// Alle Änderungen im Context rückgängig machen
        /// </summary>
        /// <param name="context"></param>
        public static void CancelChanges(this ObjectContext context)
        {

            foreach (var entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added)) 
                if (entry.Entity != null) context.DeleteObject(entry.Entity); 
                                   
            foreach (var entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified)) 
                if (entry.Entity != null) context.Refresh(RefreshMode.StoreWins, entry.Entity);                    
                                   
            foreach (var entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted)) 
                if (entry.Entity != null) context.Refresh(RefreshMode.StoreWins, entry.Entity);                    

            context.AcceptAllChanges();
        }

        /// <summary>
        /// Alle Entities neuladen (bei Langzeit-Unit of work)
        /// </summary>
        /// <param name="context"></param>
        public static void Reload(this ObjectContext context)
        {

            var loaded = context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Deleted | EntityState.Unchanged).Select(p => p.Entity);
            context.Refresh(RefreshMode.StoreWins, loaded);
            context.AcceptAllChanges();
        }

        /// <summary>
        /// Alle Entitäten eines Typs geben
        /// </summary>
        /// <typeparam name="TEntity">Entitätstyp</typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> GetEntities<TEntity>(this ObjectContext context) where TEntity : EntityObject
        {
        
            var entities = from se in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified |
                                                                                       EntityState.Deleted | EntityState.Unchanged)
                           where se.Entity is TEntity
                           select se.Entity as TEntity;
            return entities;                  
        } 
    }
}
