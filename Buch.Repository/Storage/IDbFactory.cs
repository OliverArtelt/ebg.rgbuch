﻿using System;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Storage
{
    
    public interface IDbFactory
    {
        BuchEntities CreateBuchContext();
    }
}
