﻿using System;
using System.ComponentModel.Composition;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Storage
{
   
    [Export(typeof(IDbFactory))]
    public class DbFactory : IDbFactory
    {

        [Import("ConnectionString")]
        public String ConnString { get; set; }


        public BuchEntities CreateBuchContext()
        {

            return new BuchEntities();
        }
    }
}
