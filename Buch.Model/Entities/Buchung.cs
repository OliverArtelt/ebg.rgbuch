﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;


namespace Rechnungsbuch.Entities
{
    
    [DebuggerDisplay("{BelegNr} {Nummer}")]
    public partial class Buchung //: IEquatable<Buchung>
    {

        public override string ToString()
        {
            return BelegNr.HasValue? BelegNr.ToString(): "neu";
        }
       
        public bool Equals(Buchung other)
        {
                    
            if (other == null) return false;
            return (this.IDBuchung.Equals(other.IDBuchung));
        }

        public override bool Equals(object obj)
        {

            var other = obj as Buchung;
            if (other == null) return false;
            return this.Equals(other);
        }

        public override int GetHashCode()
        {
            return IDBuchung.GetHashCode();
        }  
    }
}
