﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Diagnostics;


namespace Rechnungsbuch.Entities
{
    
    [DebuggerDisplay("{Name}")]
    public partial class Rkz : IEquatable<Rkz>
    {

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(Rkz other)
        {
                    
            if (other == null) return false;
            return (this.IDRkz.Equals(other.IDRkz));
        }

        public override bool Equals(object obj)
        {

            var other = obj as Rkz;
            if (other == null) return false;
            return this.Equals(other);
        }

        public override int GetHashCode()
        {
            return IDRkz.GetHashCode();
        }
    }
}
