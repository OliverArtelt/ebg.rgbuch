﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


namespace Rechnungsbuch.Entities
{
    
    [DebuggerDisplay("Id {KeyMandant}, {Jahr}")]
    public partial class Buchungsjahr
    {

        public override string ToString()
        {
            return Jahr.ToString();
        }
    }
}
