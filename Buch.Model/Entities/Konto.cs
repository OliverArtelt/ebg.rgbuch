﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Diagnostics;


namespace Rechnungsbuch.Entities
{
    
    [DebuggerDisplay("{KontoNr}")]
    public partial class Konto : IEquatable<Konto>
    {

        public Kontotyp Typ 
        { 
        
            get { 

                if (KeyTyp == 1) return Kontotyp.Debitor;
                if (KeyTyp == 2) return Kontotyp.Kreditor;
                if (KeyTyp == 3) return Kontotyp.Sonstige;

                return Kontotyp.Undefiniert;
            } 
        }


        public override string ToString()
        {
            return KontoNr.ToString();
        }

        public bool Equals(Konto other)
        {
                    
            if (other == null) return false;
            return (this.IDKonto.Equals(other.IDKonto));
        }

        public override bool Equals(object obj)
        {

            var other = obj as Konto;
            if (other == null) return false;
            return this.Equals(other);
        }

        public override int GetHashCode()
        {
            return IDKonto.GetHashCode();
        }
    }
}
