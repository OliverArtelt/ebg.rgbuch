﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Rechnungsbuch.Entities
{

    public enum Kontotyp
    {

        Undefiniert = 0,
        Debitor     = 1,
        Kreditor    = 2,
        Sonstige    = 3
    }
}
