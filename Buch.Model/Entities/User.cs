﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;


namespace Rechnungsbuch.Entities
{
    
    [DebuggerDisplay("{VollerName}")]
    public partial class User
    {


        public String VollerName 
        { 
            get { 
            
                if (String.IsNullOrEmpty(Vorname)) return Name;
                return String.Format("{0}, {1}", Name, Vorname);
            }
        }


        public bool HasAnyRole { get { return Roles != null && Roles.Count > 0; } }


        public bool HasRole(String name)
        {

            if (Roles == null) return false;
            return Roles.Any(p => p.Name == name);    
        }
    }
}
