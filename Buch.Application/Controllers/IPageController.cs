﻿using System;


namespace Rechnungsbuch.Application.Controllers
{
    
    interface IPageController
    {
        
        /// <summary>
        /// Controller einrichten (Modellbindung)
        /// </summary>        
        void Initialize(object context = null);
        /// <summary>
        /// Views etc öffnen
        /// </summary>
        void Run();
        /// <summary>
        /// Controller entladen
        /// </summary>
        void Shutdown();
        /// <summary>
        /// IView geben
        /// </summary>
        object PageView { get; }
        /// <summary>
        /// Controller wird einmal initialisiert aber kann mehrere Male laufen
        /// </summary>
        bool IsInitialized { get; }
    }
}
