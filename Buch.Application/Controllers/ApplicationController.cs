﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Primitives;
using System.Waf.Applications;
using Rechnungsbuch.Application.Services;
using Rechnungsbuch.Application.ViewModels;
using Rechnungsbuch.Catalog;
using Rechnungsbuch.Repositories;


namespace Rechnungsbuch.Application.Controllers
{
    
    [Export]
    public class ApplicationController : Controller, IPageController
    {

        private bool isInitialized = false;


        [Import(typeof(IUserErrorService))]
        public Lazy<IUserErrorService> ErrorService { get; set; }

        [Import(typeof(RgBuchController))]
        internal Lazy<RgBuchController> RgBuchController { get; set; }

        [Import(typeof(PeBuchController))]
        internal Lazy<PeBuchController> PeBuchController { get; set; }
        
        [Import]
        internal MainViewModel MainViewModel { get; set; }
        
        [Import]
        internal EmptyViewModel EmptyViewModel { get; set; }

        [Import]
        public IBenutzerRepository BenutzerRepository { get; set; }
       
        [Import("Application.Name")]
        public String ApplicationName { get; set; }
        [Import("Application.Version")]
        public String ApplicationVersion { get; set; }
        [Import("Application.Test")]
        public bool ApplicationIstTestSystem { get; private set; }
           

        public void Initialize(object context = null)
        {

            System.Diagnostics.Debug.WriteLine("> ApplicationController.Initialize");

            MainViewModel.ExitCommand = new DelegateCommand(Shutdown);
            MainViewModel.Title = String.Format("{0} {1} Angemeldet: {2}", ApplicationNameAndState, ApplicationVersion, 
                                                                           BenutzerRepository.Current.VollerName);
            MainViewModel.RgBuchCommand = new DelegateCommand(() => RunPageController(RgBuchController.Value, "RgBuch"),
                                                              () => BenutzerRepository.Current.HasRole("Rechnungsbuch.Schreiben"));
            MainViewModel.PeBuchCommand = new DelegateCommand(() => RunPageController(PeBuchController.Value, "PeBuch"),
                                                              () => BenutzerRepository.Current.HasRole("Posteingangsbuch.Zugriff"));
            EmptyViewModel.Message = BenutzerRepository.Current.HasAnyRole ? "Bitte wählen Sie ein Buch aus welches Sie bearbeiten möchten." :
                                                                             "Sie haben leider keine Berechtigung ein Buch zu bearbeiten.";
            MainViewModel.CurrentModuleView = EmptyViewModel.View;
            MainViewModel.TestMode = ApplicationIstTestSystem;
            isInitialized = true; 
            
            System.Diagnostics.Debug.WriteLine("< ApplicationController.Initialize");
        }


        private void RunPageController(IPageController newController, object context)
        {

            System.Diagnostics.Debug.WriteLine("> ApplicationController.RunPageController");

            try {

                if (!newController.IsInitialized) newController.Initialize(context);
                MainViewModel.CurrentModuleView = newController.PageView;
                newController.Run(); 
                MainViewModel.CurrentModule = context.ToString();  
            }
            catch (Exception x) {

                ErrorService.Value.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< ApplicationController.RunPageController");
        }

        public void Run()
        {
            
            MainViewModel.Show();
            //z.Z. nur ein Buch => Shortcut
            //if (BenutzerRepository.Current.HasRole("Rechnungsbuch.Schreiben")) 
                RunPageController(RgBuchController.Value, "RgBuch");
        }

        public void Shutdown()
        {

            isInitialized = false; 
            if (RgBuchController.IsValueCreated) RgBuchController.Value.Shutdown();
            if (PeBuchController.IsValueCreated) PeBuchController.Value.Shutdown();
            System.Windows.Application.Current.Shutdown();
        }


        private String ApplicationNameAndState
        {

            get {

                if (!ApplicationIstTestSystem) return ApplicationName;
                return String.Format("{0} Testsystem", ApplicationName); 
            }
        }


        public object PageView 
        { 
            get { throw new ApplicationException("Application controller cannot be a child controller."); } 
        }

        public bool IsInitialized
        {
            get { return isInitialized; }
        }
    }
}
