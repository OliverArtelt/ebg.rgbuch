﻿using System.ComponentModel.Composition;
using Rechnungsbuch.Application.ViewModels;
using Rechnungsbuch.Repositories;


namespace Rechnungsbuch.Application.Controllers
{
    
    [Export] 
    class PeBuchController : IPageController
    {

        private bool isInitialized = false;


        [Import]
        public PeBuchViewModel PeBuchViewModel { get; set; }
      
        
        public void Initialize(object context = null)
        {
            
            isInitialized = true; 
        }


        public void Run()
        {
        }


        public void Shutdown()
        {
            
            isInitialized = false; 
        }


        public object PageView
        {
            get { return PeBuchViewModel.View; }
        }


        public bool IsInitialized
        {
            get { return isInitialized; }
        }
    }
}
