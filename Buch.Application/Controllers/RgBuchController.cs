﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Data;
using Rechnungsbuch.Application.Services;
using Rechnungsbuch.Application.ViewModels;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Catalog;
using Rechnungsbuch.Entities;
using Rechnungsbuch.Error;
using Rechnungsbuch.Repositories;


namespace Rechnungsbuch.Application.Controllers
{
    
    [Export] 
    class RgBuchController : IPageController
    {
      
        private bool isInitialized = false;
        private Lazy<IBuchRepository> repository;
        private EditMode buchEditMode;
        private EditMode kontoEditMode;
        private BuchLockKey currentLock;

        private DelegateCommand addBuchCmd;
        private DelegateCommand editBuchCmd;
        private DelegateCommand saveBuchCmd;
        private DelegateCommand cancelBuchCmd;
        private DelegateCommand histBuchCmd;

        private DelegateCommand editKtoCmd;
        private DelegateCommand addDebKtoCmd;
        private DelegateCommand addKredKtoCmd;
        private DelegateCommand addSnstKtoCmd;
        private DelegateCommand delKtoCmd;
        private DelegateCommand takeKtoCmd;
        private DelegateCommand saveKtoCmd;
        private DelegateCommand cancelKtoCmd;

        private DelegateCommand addRkzCmd;
        private DelegateCommand delRkzCmd;
        private DelegateCommand takeRkzCmd;

        private Buchung editedBuchung;


        [Import]
        public IUserErrorService ErrorService { get; set; }
        [Import]
        public RgBuchViewModel BuchViewModel { get; set; }
        [Import]
        public RgBuchListViewModel BuchListViewModel { get; set; }
        [Import]
        public RgBuchDetailViewModel BuchDetailViewModel { get; set; }
        [Import]
        public RgBuchHistoryViewModel BuchHistoryViewModel { get; set; }
        [Import]
        public KontoViewModel KontoViewModel { get; set; }
        [Import]
        public RkzViewModel RkzViewModel { get; set; }
        [Import]
        public IPartProvider Factory { get; set; }
        [Import(typeof(IMandantRepository))]
        public Lazy<IMandantRepository> MandantRepository { get; set; }

 
#region I N I T I A L I Z I N G 
        
        public void Initialize(object context = null)
        {
            
            BuchViewModel.BuchDetailView = BuchDetailViewModel.View;
            BuchViewModel.BuchListView = BuchListViewModel.View;
            BuchViewModel.KontoView = KontoViewModel.View;
            BuchViewModel.RkzView = RkzViewModel.View;

            addBuchCmd    = new DelegateCommand(AddBuchung, CanAddBuchung);
            editBuchCmd   = new DelegateCommand(EditBuchung, CanEditBuchung);
            saveBuchCmd   = new DelegateCommand(SaveBuchung, CanSaveBuchung);
            cancelBuchCmd = new DelegateCommand(CancelBuchung, CanCancelBuchung);
            histBuchCmd   = new DelegateCommand(OpenHistory, () => BuchDetailViewModel.Current != null);

            addDebKtoCmd  = new DelegateCommand(() => AddKonto(Kontotyp.Debitor), CanAddKonto);
            addKredKtoCmd = new DelegateCommand(() => AddKonto(Kontotyp.Kreditor), CanAddKonto);
            addSnstKtoCmd = new DelegateCommand(() => AddKonto(Kontotyp.Sonstige), CanAddKonto);
            delKtoCmd     = new DelegateCommand(DeleteKonto, CanDeleteKonto);
            editKtoCmd    = new DelegateCommand(EditKonto, CanEditKonto);
            takeKtoCmd    = new DelegateCommand(TakeKonto, () => KontoViewModel.Current != null);
            saveKtoCmd    = new DelegateCommand(SaveKonto, CanSaveKonto);
            cancelKtoCmd  = new DelegateCommand(CancelKonto, CanCancelKonto);

            addRkzCmd     = new DelegateCommand(AddRkz);
            delRkzCmd     = new DelegateCommand(DeleteRkz, () => RkzViewModel.Current != null);
            takeRkzCmd    = new DelegateCommand(TakeRkz, () => RkzViewModel.Current != null);

            BuchDetailViewModel.AddCommand = addBuchCmd;
            BuchDetailViewModel.EditCommand = editBuchCmd;
            BuchDetailViewModel.SaveCommand = saveBuchCmd;
            BuchDetailViewModel.CancelCommand = cancelBuchCmd;
            BuchDetailViewModel.HistoryCommand = histBuchCmd;
            BuchDetailViewModel.Mandanten = MandantRepository.Value.MandantenMitKontenplan;

            int ndx = Properties.Settings.Default.Mandant;
            BuchDetailViewModel.CurrentMandant = MandantRepository.Value.MandantenMitKontenplan.FirstOrDefault(p => p.IdMandant == ndx);
            if (BuchDetailViewModel.CurrentMandant != null) {

                ndx = Properties.Settings.Default.Jahr;
                BuchDetailViewModel.CurrentJahr = BuchDetailViewModel.CurrentMandant.Buchungsjahre.FirstOrDefault(p => p.Jahr == ndx);
            }

            RkzViewModel.TakeCommand = takeRkzCmd;
            RkzViewModel.AddCommand = addRkzCmd;
            RkzViewModel.DeleteCommand = delRkzCmd;
            KontoViewModel.TakeCommand = takeKtoCmd;
            KontoViewModel.AddDebCommand = addDebKtoCmd;
            KontoViewModel.AddKredCommand = addKredKtoCmd;
            KontoViewModel.AddSnstCommand = addSnstKtoCmd;
            KontoViewModel.DeleteCommand = delKtoCmd;
            KontoViewModel.EditCommand = editKtoCmd;
            KontoViewModel.SaveCommand = saveKtoCmd;
            KontoViewModel.CancelCommand = cancelKtoCmd;

            BuchListViewModel.PropertyChanged += BuchListItemChanged;
            KontoViewModel.PropertyChanged += KontoListItemChanged;
            RkzViewModel.PropertyChanged += RkzListItemChanged;
            BuchDetailViewModel.PropertyChanged += MandantChanged;

            LoadData();
            isInitialized = true; 
        }


        public void Run()
        {
            
            buchEditMode = EditMode.Read;
            kontoEditMode = EditMode.Read;
            RaiseEditModeChanged();
            BuchDetailViewModel.SetFocus(Views.FocusTarget.FirstCommand);
        }


        public void Shutdown()
        {

            isInitialized = false; 

            try {

                if (currentLock != null) repository.Value.ReleaseLock(currentLock);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);    
            }
        }


        public object PageView { get { return BuchViewModel.View; } }


        public bool IsInitialized { get { return isInitialized; } }


#endregion

#region D A T A   H A N D L I N G 


        private void SaveChanges()
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.SaveChanges");

            
            try {

                using (new Hourglass()) {
                
                    repository.Value.Save();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            LoadData(true);

            System.Diagnostics.Debug.WriteLine("< RgBuchController.SaveChanges");
            
        }


        private void LoadData(bool tryRestore = false)
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.LoadData");


            try {

                var kto = KontoViewModel.Current;
                var rkz = RkzViewModel.Current;
                var bch = BuchListViewModel.Current;
            
                if (BuchDetailViewModel.CurrentMandant == null) return;
                if (BuchDetailViewModel.CurrentJahr == null) return;


                using (new Hourglass()) {

                    if (repository != null) Factory.Destroy(repository);
                    repository = Factory.CreateLazy<IBuchRepository>();
                    repository.Value.LoadMandant(BuchDetailViewModel.CurrentJahr);

                    BuchListViewModel.Buchungen = CollectionViewSource.GetDefaultView(repository.Value.Buchungen);
                    KontoViewModel.Konten = CollectionViewSource.GetDefaultView(repository.Value.Konten);
                    RkzViewModel.RkzList = CollectionViewSource.GetDefaultView(repository.Value.RkzSet);
                    BuchListViewModel.OwnEdited = repository.Value.ModifiedBy;
                }

                if (tryRestore) {

                    if (kto != null) KontoViewModel.Current    = repository.Value.Konten.FirstOrDefault(p => p.IDKonto == kto.IDKonto);
                    if (rkz != null) RkzViewModel.Current      = repository.Value.RkzSet.FirstOrDefault(p => p.IDRkz == rkz.IDRkz);
                    if (bch != null) BuchListViewModel.Current = repository.Value.Buchungen.FirstOrDefault(p => p.IDBuchung == bch.IDBuchung);
        
                } else {

                    BuchListViewModel.Current = repository.Value.Buchungen.LastOrDefault();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< RgBuchController.LoadData");
        }


#endregion

#region P R O P E R T Y   C H A N G E D


        private void MandantChanged(object sender, PropertyChangedEventArgs e)
        {
           
            try {
             
                if (e.PropertyName == "CurrentMandant" || e.PropertyName == "CurrentJahr") {

                    if (BuchDetailViewModel.CurrentMandant == null || BuchDetailViewModel.CurrentJahr == null) {

                        BuchListViewModel.Buchungen = null;
                        KontoViewModel.Konten = null;
                    
                    } else {

                        Properties.Settings.Default.Mandant = BuchDetailViewModel.CurrentMandant.IdMandant;
                        Properties.Settings.Default.Jahr = BuchDetailViewModel.CurrentJahr.Jahr;
                        Properties.Settings.Default.Save();
                    }

                    BuchDetailViewModel.Current = null;
                    RaiseEditModeChanged();
                    LoadData();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void BuchListItemChanged(object sender, PropertyChangedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.BuchListItemChanged");

           
            try {
             
                if (e.PropertyName == "Current") {

                    if (BuchListViewModel.Current == null) return;

                    Buchung bg = BuchListViewModel.Current;
                    BuchDetailViewModel.Current = bg;
                    RaiseEditModeChanged();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< RgBuchController.BuchListItemChanged");

        }


        private void KontoListItemChanged(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "Current") {

                RaiseEditModeChanged();
            }

            if (e.PropertyName == "InEditMode") {

                if (KontoViewModel.InEditMode == false) {
                    
                    SaveChanges();
                    KontoViewModel.Added.Clear();
                }
            }
        }


        private void RkzListItemChanged(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "Current") {

                RaiseEditModeChanged();
            }

            if (e.PropertyName == "InEditMode") {

                if (RkzViewModel.InEditMode == false) {
                   
                    SaveChanges();
                    RkzViewModel.Added.Clear();
                }
            }
        }


#endregion

#region  C A N   E X E C U T E
          

        private bool CanAddBuchung()
        {

            return buchEditMode == EditMode.Read && BuchDetailViewModel.CurrentJahr != null;
        }


        private bool CanEditBuchung()
        {

            return buchEditMode == EditMode.Read && BuchDetailViewModel.Current != null;
        }


        private bool CanEditKonto()
        {

            return kontoEditMode == EditMode.Read && KontoViewModel.Current != null;
        }


        private bool CanDeleteKonto()
        {

            return kontoEditMode == EditMode.Read && KontoViewModel.Current != null;
        }


        private bool CanAddKonto()
        {

            return kontoEditMode == EditMode.Read && BuchDetailViewModel.CurrentJahr != null;
        }

        
        private bool CanSaveBuchung()
        {

            return buchEditMode != EditMode.Read;
        }

        
        private bool CanSaveKonto()
        {

            return kontoEditMode != EditMode.Read;
        }

        
        private bool CanCancelBuchung()
        {

            return buchEditMode != EditMode.Read;
        }

        
        private bool CanCancelKonto()
        {

            return kontoEditMode != EditMode.Read;
        }

        
        private void RaiseEditModeChanged()
        {

            addBuchCmd.RaiseCanExecuteChanged();
            editBuchCmd.RaiseCanExecuteChanged();
            saveBuchCmd.RaiseCanExecuteChanged();
            cancelBuchCmd.RaiseCanExecuteChanged();
            editKtoCmd.RaiseCanExecuteChanged();
            addDebKtoCmd.RaiseCanExecuteChanged();
            addKredKtoCmd.RaiseCanExecuteChanged();
            addSnstKtoCmd.RaiseCanExecuteChanged();
            delKtoCmd.RaiseCanExecuteChanged();
            saveKtoCmd.RaiseCanExecuteChanged();
            cancelKtoCmd.RaiseCanExecuteChanged();
            takeKtoCmd.RaiseCanExecuteChanged();
            takeRkzCmd.RaiseCanExecuteChanged();
            histBuchCmd.RaiseCanExecuteChanged();

            BuchDetailViewModel.IsReadOnly = buchEditMode == EditMode.Read;
            BuchListViewModel.IsEnabled = buchEditMode == EditMode.Read;
            KontoViewModel.InEditMode = kontoEditMode != EditMode.Read;
            BuchDetailViewModel.SwitchIsEnabled = buchEditMode == EditMode.Read && kontoEditMode == EditMode.Read;
            BuchListViewModel.FilterEnabled = BuchListViewModel.Buchungen != null;
            KontoViewModel.FilterEnabled = KontoViewModel.Konten != null;
        }


#endregion

#region B U C H U N G

        
        private void AddBuchung()
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.AddBuchung");


            try {

                using (new Hourglass()) {

                    SaveChanges();
                    editedBuchung = repository.Value.NeueBuchung();

                    var buchlock = repository.Value.AcquireLock(editedBuchung);
                    if (!buchlock.IsSuccessful) {

                        throw new ApplicationException("Interner Fehler: Die neue Buchung kann nicht bearbeitet werden, da sie gesperrt ist.");
                    }
                    
                    currentLock = buchlock;

                    if (KontoViewModel.Current != null) editedBuchung.Konto = repository.Value.Konten.FirstOrDefault(p => p.IDKonto == KontoViewModel.Current.IDKonto);
                    if (RkzViewModel.Current != null)   editedBuchung.Rkz   = repository.Value.RkzSet.FirstOrDefault(p => p.IDRkz == RkzViewModel.Current.IDRkz);

                    BuchListViewModel.Current = editedBuchung;
                    BuchListViewModel.Refresh(true);
                    BuchDetailViewModel.SetFocus(Views.FocusTarget.FirstInputControl);            
 
                    buchEditMode = EditMode.Add;
                    RaiseEditModeChanged();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
 
            System.Diagnostics.Debug.WriteLine("< RgBuchController.AddBuchung");

       }

        
        private void EditBuchung()
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.EditBuchung");

            editedBuchung = BuchDetailViewModel.Current;
            if (editedBuchung == null) return; 

            try {

                var buchlock = repository.Value.AcquireLock(BuchDetailViewModel.Current);
                if (!buchlock.IsSuccessful) {
                
                    var bld = new StringBuilder();
                    bld.AppendLine("Die Buchung kann nicht bearbeitet werden, da sie bereits zur Bearbeitung gesperrt ist.")
                       .Append("Gesperrt seit: ").AppendLine(buchlock.Sperrzeit.AsHumanReadableText())
                       .Append("Bearbeiter: ").AppendLine(buchlock.Benutzer)
                       .Append("Arbeitsplatz: ").AppendLine(buchlock.Station);
                    throw new UserException(bld.ToString());
                }
                currentLock = buchlock;

                using (new Hourglass()) {

                    SaveChanges();
                }

                if (editedBuchung.KeyKonto == 0 && KontoViewModel.Current != null) 
                    BuchDetailViewModel.Konto = repository.Value.Konten.FirstOrDefault(p => p.IDKonto == KontoViewModel.Current.IDKonto);
                if (editedBuchung.KeyRKZ == 0 && RkzViewModel.Current != null)   
                    BuchDetailViewModel.Rkz = repository.Value.RkzSet.FirstOrDefault(p => p.IDRkz == RkzViewModel.Current.IDRkz);
                BuchDetailViewModel.SetFocus(Views.FocusTarget.FirstInputControl);

                buchEditMode = EditMode.Edit;
                RaiseEditModeChanged();
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< RgBuchController.EditBuchung");

        }

        
        private void SaveBuchung()
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.SaveBuchung");


            try {

                repository.Value.ReleaseLock(currentLock);
                currentLock = null;

                SaveChanges();

                buchEditMode = EditMode.Read;
                RaiseEditModeChanged();

                if (editedBuchung != null) BuchListViewModel.Current = repository.Value.Buchungen.FirstOrDefault(p => p.IDBuchung == editedBuchung.IDBuchung);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< RgBuchController.SaveBuchung");

        }

        
        private void CancelBuchung()
        {

            System.Diagnostics.Debug.WriteLine("> RgBuchController.CancelBuchung");


            try {

                repository.Value.ReleaseLock(currentLock);
                currentLock = null;

                LoadData(true);

                buchEditMode = EditMode.Read;
                RaiseEditModeChanged();

                if (editedBuchung != null) BuchListViewModel.Current = repository.Value.Buchungen.FirstOrDefault(p => p.IDBuchung == editedBuchung.IDBuchung);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }

            System.Diagnostics.Debug.WriteLine("< RgBuchController.CancelBuchung");

        }


        private void OpenHistory()
        {

            Lazy<RgBuchHistoryViewModel> exp = null;
             
            try {
             
                exp = Factory.CreateLazy<RgBuchHistoryViewModel>();
                exp.Value.Buchung = BuchDetailViewModel.Current;
                exp.Value.ShowModal();
            }
            finally {

                Factory.Destroy(exp); 
            }
        }


#endregion

#region K O N T O


        private void TakeKonto()
        {

            try {

                if (KontoViewModel.Current == null) return;
                if (BuchDetailViewModel.Current == null) return;
                if (KontoViewModel.InEditMode) return;
                if (buchEditMode == EditMode.Read) return;

                BuchDetailViewModel.Konto = repository.Value.Konten.FirstOrDefault(p => p.IDKonto == KontoViewModel.Current.IDKonto);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void AddKonto(Kontotyp typ)
        {

            try {

                using (new Hourglass()) {

                    kontoEditMode = EditMode.Add;
                    RaiseEditModeChanged();

                    var kto = repository.Value.NeuesKonto(typ);
                    KontoViewModel.Added.Add(kto);
                    KontoViewModel.Current = kto;          
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void DeleteKonto()
        {
 
            try {
     
                if (KontoViewModel.Current == null) return;
                repository.Value.Konten.Remove(KontoViewModel.Current);

                SaveChanges();
                KontoViewModel.Refresh();
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void EditKonto()
        {
 
            try {
                
                kontoEditMode = EditMode.Edit;
                RaiseEditModeChanged();
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }
         
        
        private void SaveKonto()
        {

            try {
                
                SaveChanges();

                kontoEditMode = EditMode.Read;
                RaiseEditModeChanged();
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }

        
        private void CancelKonto()
        {

            try {

                using (new Hourglass()) {
                
                    LoadData(true);

                    kontoEditMode = EditMode.Read;
                    KontoViewModel.InEditMode = false;

                    RaiseEditModeChanged();
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }
        

#endregion

#region R K Z


        private void TakeRkz()
        {

            try {

                if (RkzViewModel.Current == null) return;
                if (BuchDetailViewModel.Current == null) return;
                if (buchEditMode == EditMode.Read) return;

                BuchDetailViewModel.Rkz = repository.Value.RkzSet.FirstOrDefault(p => p.IDRkz == RkzViewModel.Current.IDRkz);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void AddRkz()
        {

            try {

                using (new Hourglass()) {

                    var rkz = repository.Value.NeueRkz();
                    RkzViewModel.Added.Add(rkz);
                    RkzViewModel.Current = rkz; 
                    RkzViewModel.Refresh();                         
                }
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


        private void DeleteRkz()
        {

            try {

                if (RkzViewModel.Current == null) return;
                repository.Value.RkzSet.Remove(RkzViewModel.Current);
            }
            catch (Exception x) {

                ErrorService.ShowAndLogException(x);
            }
        }


#endregion
         

    }
}
