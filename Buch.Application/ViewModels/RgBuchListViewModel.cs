﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class RgBuchListViewModel : ViewModel<IRgBuchListView>
    {           

        public ICommand ShowAllCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }

        
        /// <summary>
        /// Liste der ID's aller Buchungen die vom aktuellen Bearbeiter 'angefaßt' wurden. 
        /// Muß aufsteigend sortiert sein.
        /// </summary>
        public int[] OwnEdited { get; set; }

        
        [ImportingConstructor]
        public RgBuchListViewModel(IRgBuchListView view) : base(view)
        {

            ShowAllCommand = new DelegateCommand(SelectAll);
            SearchCommand = new DelegateCommand(() => Refresh(false));
            searchAll = true;
        }
         
         
        private bool FilterPredicate(object item)
        {

            var bg = item as Buchung;
            if (bg == null) return false;

            if (nurEigene) {

                if (OwnEdited == null) return false;
                if (Array.BinarySearch(OwnEdited, bg.IDBuchung) < 0) return false;
            }

            if (String.IsNullOrEmpty(filterText)) return true;
            String filter = filterText.ToLower();

            if (bg.Konto != null && (searchAll || searchKonto)) {

                if (bg.Konto.KontoNr.ToString().Contains(filter)) return true;
            }

            if (bg.Konto != null && (searchAll || searchText)) {

                if (!String.IsNullOrEmpty(bg.Konto.Name) && bg.Konto.Name.ToLower().Contains(filter)) return true;
            }

            if ((searchAll || searchText) && bg.Rkz != null) {

                if (!String.IsNullOrEmpty(bg.Rkz.Name) && bg.Rkz.Name.ToLower().Contains(filter)) return true;
            }
            
            if ((searchAll || searchRgNr) && !String.IsNullOrEmpty(bg.Nummer) && bg.Nummer.ToLower().Contains(filter)) return true;
            if ((searchAll || searchText) && !String.IsNullOrEmpty(bg.Bemerkungen) && bg.Bemerkungen.ToLower().Contains(filter)) return true;
            if ((searchAll || searchBeleg) && bg.BelegNr.HasValue && bg.BelegNr.ToString().Contains(filter)) return true;
            if ((searchAll || searchBetrag) && bg.Betrag.ToString().Contains(filter)) return true;

            return false;
        }


        private void SelectAll()
        {

            nurEigene = false;
            filterText = String.Empty;

            RaisePropertyChanged("FilterText");
            RaisePropertyChanged("NurEigene");
            buchungen.Refresh();
        }


        public void Refresh(bool resort = false)
        {

            if (resort) {

                var view = buchungen as ListCollectionView;
                if (view != null && view.CanSort)
                    view.SortDescriptions.Add(new SortDescription("BelegNr", ListSortDirection.Descending));
                    
                RaisePropertyChanged("Buchungen");
            }

            if (buchungen != null) buchungen.Refresh();
            if (current != null) ViewCore.DisplayItem(current);
        }


#region P R O P E R T I E S
                  
 
        private ICollectionView buchungen;

        public ICollectionView Buchungen 
        { 
            
            get { return buchungen; }
            set { 
            
                if (buchungen != value) {
                    
                    buchungen = value;
                    RaisePropertyChanged("Buchungen");
                    if (buchungen == null) return;

                    buchungen.Filter = FilterPredicate;
                    Refresh(true);
                }
            }
        }

         
        private Buchung current;
        
        public Buchung Current
        { 
            
            get { return current; }
            set { 
            
                if (current != value) // && value != null)
                {
                    current = value;
                    RaisePropertyChanged("Current");
                    if (current != null) ViewCore.DisplayItem(current);
                }
            }
        }


        private String filterText;
                  
        public String FilterText 
        { 
            
            get { return filterText; }
            set { 
            
                if (filterText != value)
                {
                    filterText = value.Trim();
                    RaisePropertyChanged("FilterText");
                }
            }
        }

        private bool nurEigene; 

        public bool NurEigene 
        { 
            
            get { return nurEigene; }
            set { 
            
                if (nurEigene != value)
                {
                    nurEigene = value;
                    RaisePropertyChanged("NurEigene");
                    buchungen.Refresh();
                }
            }
        }

        private bool isEnabled; 

        public bool IsEnabled 
        { 
            
            get { return isEnabled; }
            set { 
            
                if (isEnabled != value)
                {
                    isEnabled = value;                  
                    RaisePropertyChanged("IsEnabled");
                }
            }
        }
        

        private bool filterEnabled;
        
        public bool FilterEnabled 
        { 
            
            get { return filterEnabled; }
            set { 
            
                if (filterEnabled != value)
                {
                    filterEnabled = value;
                    RaisePropertyChanged("FilterEnabled");
                    RaisePropertyChanged("ShowLocker");
                    Refresh();
                }
            }
        }

        public bool ShowLocker { get { return !FilterEnabled; } }

        
        private bool searchAll;

        public bool SearchAll 
        { 
            
            get { return searchAll; }
            set { 
            
                if (searchAll != value) {

                    searchAll = value;
                    RaisePropertyChanged("SearchAll");

                    if (searchAll) {

                        searchBeleg  = false;
                        searchKonto  = false;
                        searchRgNr   = false;
                        searchBetrag = false;
                        searchText   = false;
                        RaisePropertyChanged("SearchBeleg");
                        RaisePropertyChanged("SearchKonto");
                        RaisePropertyChanged("SearchRgNr");
                        RaisePropertyChanged("SearchBetrag");
                        RaisePropertyChanged("SearchText");
                    }

                    buchungen.Refresh();
                }
            }
        }

        
        private bool searchBeleg;

        public bool SearchBeleg 
        { 
            
            get { return searchBeleg; }
            set { 
            
                if (searchBeleg != value) {

                    searchBeleg = value;
                    RaisePropertyChanged("SearchBeleg");

                    if (searchBeleg) {

                        searchAll = false;
                        RaisePropertyChanged("SearchAll");
                    }

                    buchungen.Refresh();
                }
            }
        }

        
        private bool searchKonto;

        public bool SearchKonto 
        { 
            
            get { return searchKonto; }
            set { 
            
                if (searchKonto != value)
                {
                    searchKonto = value;
                    RaisePropertyChanged("SearchKonto");

                    if (searchKonto) {

                        searchAll = false;
                        RaisePropertyChanged("SearchAll");
                    }

                    buchungen.Refresh();
                }
            }
        }

        
        private bool searchRgNr;

        public bool SearchRgNr 
        { 
            
            get { return searchRgNr; }
            set { 
            
                if (searchRgNr != value)
                {
                    searchRgNr = value;
                    RaisePropertyChanged("SearchRgNr");

                    if (searchRgNr) {

                        searchAll = false;
                        RaisePropertyChanged("SearchAll");
                    }

                    buchungen.Refresh();
                }
            }
        }

        
        private bool searchBetrag;

        public bool SearchBetrag 
        { 
            
            get { return searchBetrag; }
            set { 
            
                if (searchBetrag != value)
                {
                    searchBetrag = value;
                    RaisePropertyChanged("SearchBetrag");

                    if (searchBetrag) {

                        searchAll = false;
                        RaisePropertyChanged("SearchAll");
                    }

                    buchungen.Refresh();
                }
            }
        }

        
        private bool searchText;

        public bool SearchText 
        { 
            
            get { return searchText; }
            set { 
            
                if (searchText != value)
                {
                    searchText = value;
                    RaisePropertyChanged("SearchText");

                    if (searchText) {

                        searchAll = false;
                        RaisePropertyChanged("SearchAll");
                    }

                    buchungen.Refresh();
                }
            }
        }


#endregion

    }
}
