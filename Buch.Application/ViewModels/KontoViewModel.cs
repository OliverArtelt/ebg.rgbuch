﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class KontoViewModel : ViewModel<IKontoView>
    {

        private String filterText;
        private bool nurDebitoren;
        private bool nurKreditoren;
        private bool nurSonstige;
        
        private ICollectionView konten;
        private Konto current;
        
        private ICommand editCommand;
        private ICommand takeCommand;
        private ICommand addDebCommand;
        private ICommand addKredCommand;
        private ICommand addSnstCommand;
        private ICommand deleteCommand;
        private ICommand saveCommand;
        private ICommand cancelCommand;
        private bool     inEditMode;


        public ICommand ShowAllCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }
        /// <summary>
        /// Anfügen bei gefilterter CollectionView: angefügte Konten nicht rausfiltern
        /// </summary> 
        public List<Konto> Added { get; set; }
         
        
        [ImportingConstructor]
        public KontoViewModel(IKontoView view) : base(view)
        {

            ShowAllCommand = new DelegateCommand(SelectAll);
            SearchCommand = new DelegateCommand(Refresh);
            nurDebitoren = true;
            nurKreditoren = true;
            nurSonstige = true;
            inEditMode = false;
            Added = new List<Konto>();
        }

        
        private bool FilterPredicate(object item)
        {

            var kto = item as Konto;
            if (kto == null) return false;
            if (kto.IDKonto == 0) return false;

            if (Added != null && Added.Contains(kto)) return true;

            if (kto.Typ == Kontotyp.Debitor  && !NurDebitoren)  return false;
            if (kto.Typ == Kontotyp.Kreditor && !NurKreditoren) return false;
            if (kto.Typ == Kontotyp.Sonstige && !NurSonstige)   return false;

            if (String.IsNullOrEmpty(filterText)) return true;
            String filter = filterText.ToLower();
            if (!String.IsNullOrEmpty(kto.Name) && kto.Name.ToLower().Contains(filter)) return true;
            if (kto.KontoNr.ToString().Contains(filter)) return true;
            if (!String.IsNullOrEmpty(kto.Verwendung) && kto.Verwendung.ToLower().Contains(filter)) return true;
            
            return false;
        }


        private void SelectAll()
        {

            NurDebitoren = true;
            NurKreditoren = true;
            NurSonstige = true;
            FilterText = String.Empty;
            Refresh();
        }


        public void Refresh()
        {

            if (konten != null) konten.Refresh();
            ViewCore.DisplayItem(current);
        }


#region P R O P E R T I E S
                  
        /// <summary>
        /// Control ist bearbeitbar (Konto bearbeiten statt übernehmen)
        /// </summary>
        public bool InEditMode
        { 
            
            get { return inEditMode; }
            set { 
            
                if (inEditMode != value) {

                    inEditMode = value;
                    RaisePropertyChanged("InEditMode");
                    RaisePropertyChanged("IsReadOnly");
                    RaisePropertyChanged("EditCommandVisiblity");
                    RaisePropertyChanged("ReadCommandVisiblity");
                    
                    if (inEditMode) ViewCore.SetEditCursor();
                    else            ViewCore.DisplayItem(current);
                }
            }
        }

        /// <summary>
        /// Befehle, die nur im Editmode sichtbar sind, ein-/ausblenden
        /// </summary>
        public String EditCommandVisiblity { get { return InEditMode? "Visible": "Collapsed"; } }
        public String ReadCommandVisiblity { get { return !InEditMode? "Visible": "Collapsed"; } }
        public bool IsReadOnly { get { return !InEditMode; } }


        /// <summary>
        /// Konto in aktuelle Buchung übernehmen
        /// </summary>
        public ICommand TakeCommand 
        { 
            
            get { return takeCommand; }
            set { 
            
                if (takeCommand != value)
                {
                    takeCommand = value;
                    RaisePropertyChanged("TakeCommand");
                }
            }
        }

        /// <summary>
        /// Konto bearbeiten
        /// </summary>
        public ICommand EditCommand 
        { 
            
            get { return editCommand; }
            set { 
            
                if (editCommand != value)
                {
                    editCommand = value;
                    RaisePropertyChanged("EditCommand");
                }
            }
        }

        /// <summary>
        /// neues Debitorenkonto (Kontonummer vorbelegen)
        /// </summary>
        public ICommand AddDebCommand 
        { 
            
            get { return addDebCommand; }
            set { 
            
                if (addDebCommand != value)
                {
                    addDebCommand = value;
                    RaisePropertyChanged("AddDebCommand");
                }
            }
        }

        /// <summary>
        /// neues Kreditorenkonto (Kontonummer vorbelegen)
        /// </summary>
        public ICommand AddKredCommand 
        { 
            
            get { return addKredCommand; }
            set { 
            
                if (addKredCommand != value)
                {
                    addKredCommand = value;
                    RaisePropertyChanged("AddKredCommand");
                }
            }
        }

        /// <summary>
        /// neues Sachkonto (Kontonummer vorbelegen)
        /// </summary>
        public ICommand AddSnstCommand 
        { 
            
            get { return addSnstCommand; }
            set { 
            
                if (addSnstCommand != value)
                {
                    addSnstCommand = value;
                    RaisePropertyChanged("AddSnstCommand");
                }
            }
        }

        /// <summary>
        /// Konto löschen
        /// </summary>
        public ICommand DeleteCommand 
        { 
            
            get { return deleteCommand; }
            set { 
            
                if (deleteCommand != value)
                {
                    deleteCommand = value;
                    RaisePropertyChanged("DeleteCommand");
                }
            }
        }


        public ICommand SaveCommand 
        { 
            
            get { return saveCommand; }
            set { 
            
                if (saveCommand != value)
                {
                    saveCommand = value;
                    RaisePropertyChanged("SaveCommand");
                }
            }
        }


        public ICommand CancelCommand 
        { 
            
            get { return cancelCommand; }
            set { 
            
                if (cancelCommand != value)
                {
                    cancelCommand = value;
                    RaisePropertyChanged("CancelCommand");
                }
            }
        }

        /// <summary>
        /// Liste aller Konten
        /// </summary>
        public ICollectionView Konten 
        { 
            
            get { return konten; }
            set { 
            
                if (konten != value) {
                    
                    konten = value;
                    RaisePropertyChanged("Konten");

                    if (konten == null) return;
                    konten.Filter = FilterPredicate;
                    if (!inEditMode) konten.SortDescriptions.Add(new SortDescription("KontoNr", ListSortDirection.Ascending));
                    Refresh();
                }
            }
        }
           
        /// <summary>
        /// aktuell gewähltes Konto
        /// </summary>
        public Konto Current 
        { 
            
            get { return current; }
            set { 
            
                if (current != value)
                {
                    current = value;
                    RaisePropertyChanged("Current");
                }
            }
        }
           
        public String FilterText 
        { 
            
            get { return filterText; }
            set { 
            
                if (filterText != value)
                {
                    filterText = value.Trim();
                    RaisePropertyChanged("FilterText");
                }
            }
        }

        public bool NurKreditoren 
        { 
            
            get { return nurKreditoren; }
            set { 
            
                if (nurKreditoren != value)
                {
                    nurKreditoren = value;
                    RaisePropertyChanged("NurKreditoren");
                    Refresh();
                }
            }
        }

        public bool NurDebitoren 
        { 
            
            get { return nurDebitoren; }
            set { 
            
                if (nurDebitoren != value)
                {
                    nurDebitoren = value;
                    RaisePropertyChanged("NurDebitoren");
                    Refresh();
                }
            }
        }

        public bool NurSonstige 
        { 
            
            get { return nurSonstige; }
            set { 
            
                if (nurSonstige != value)
                {
                    nurSonstige = value;
                    RaisePropertyChanged("NurSonstige");
                    Refresh();
                }
            }
        }
        
        private bool filterEnabled;
        
        public bool FilterEnabled 
        { 
            
            get { return filterEnabled; }
            set { 
            
                if (filterEnabled != value)
                {
                    filterEnabled = value;
                    RaisePropertyChanged("FilterEnabled");
                    RaisePropertyChanged("ShowLocker");
                    Refresh();
                }
            }
        }

        public bool ShowLocker { get { return !FilterEnabled; } }


#endregion

    }
}
