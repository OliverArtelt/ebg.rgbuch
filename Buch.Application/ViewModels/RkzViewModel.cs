﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class RkzViewModel : ViewModel<IRkzView>
    {

        private String filterText;
        private ICollectionView rkzList;
        private Rkz current;
        private ICommand takeCommand;
        private ICommand addCommand;
        private ICommand deleteCommand;
        private bool inEditMode;

                
        public ICommand ShowAllCommand { get; private set; }
        /// <summary>
        /// Anfügen bei gefilterter CollectionView: angefügte Konten nicht rausfiltern
        /// </summary> 
        public List<Rkz> Added { get; set; }

        
        [ImportingConstructor]
        public RkzViewModel(IRkzView view) : base(view)
        {

            ShowAllCommand = new DelegateCommand(SelectAll);
            Added = new List<Rkz>();
        }


        private bool FilterPredicate(object item)
        {

            var rkz = item as Rkz;
            if (rkz == null) return false;
            if (rkz.IDRkz == 0) return false;
            if (rkz.NoShow) return false;

            if (Added != null && Added.Contains(rkz)) return true;

            if (String.IsNullOrEmpty(filterText)) return true;
            String filter = filterText.ToLower();
            if (!String.IsNullOrEmpty(rkz.Name) && rkz.Name.ToLower().Contains(filter)) return true;
            
            return false;
        }


        private void SelectAll()
        {

            FilterText = String.Empty;
        }


        public void Refresh()
        {

            rkzList.Refresh();
        }


#region P R O P E R T I E S
        

        /// <summary>
        /// Control ist bearbeitbar (Konto bearbeiten statt übernehmen)
        /// </summary>
        public bool InEditMode
        { 
            
            get { return inEditMode; }
            set { 
            
                if (inEditMode != value) {

                    inEditMode = value;
                    RaisePropertyChanged("InEditMode");
                    RaisePropertyChanged("IsReadOnly");
                    RaisePropertyChanged("EditCommandVisiblity");
                }
            }
        }

        /// <summary>
        /// Befehle, die nur im Editmode sichtbar sind, ein-/ausblenden
        /// </summary>
        public String EditCommandVisiblity
        {

            get {

                return InEditMode? "Visible": "Collapsed";
            }
        }

        public bool IsReadOnly { get { return !InEditMode; } }
        
        
        public ICommand TakeCommand 
        { 
            
            get { return takeCommand; }
            set { 
            
                if (takeCommand != value)
                {
                    takeCommand = value;
                    RaisePropertyChanged("TakeCommand");
                }
            }
        }

        public ICommand AddCommand 
        { 
            
            get { return addCommand; }
            set { 
            
                if (addCommand != value)
                {
                    addCommand = value;
                    RaisePropertyChanged("AddCommand");
                }
            }
        }

        public ICommand DeleteCommand 
        { 
            
            get { return deleteCommand; }
            set { 
            
                if (deleteCommand != value)
                {
                    deleteCommand = value;
                    RaisePropertyChanged("DeleteCommand");
                }
            }
        }

        public ICollectionView RkzList 
        { 
            
            get { return rkzList; }
            set { 
            
                if (rkzList != value)
                {
                    rkzList = value;
                    rkzList.Filter = FilterPredicate;
                    rkzList.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
                    rkzList.Refresh();
                    RaisePropertyChanged("RkzList");
                }
            }
        }
           
        public Rkz Current 
        { 
            
            get { return current; }
            set { 
            
                if (current != value)
                {
                    current = value;
                    RaisePropertyChanged("Current");
                }
            }
        }
                
        public String FilterText 
        { 
            
            get { return filterText; }
            set { 
            
                if (filterText != value) {

                    filterText = value.Trim();
                    RaisePropertyChanged("FilterText");
                    rkzList.Refresh();
                }
            }
        }

#endregion

    }
}
