﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class ImpressumViewModel : ViewModel<IImpressumView>
    {
    
        [Import("Application.Version")]
        public String ApplicationVersion { get; set; }

        
        [ImportingConstructor]
        public ImpressumViewModel(IImpressumView view) : base(view)
        {
        }
    }
}
