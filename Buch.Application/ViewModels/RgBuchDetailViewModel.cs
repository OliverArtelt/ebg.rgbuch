﻿using System;
using System.Linq;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Entities;
using System.Collections;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class RgBuchDetailViewModel : ViewModel<IRgBuchDetailView>
    {
        
        private Buchung current;
        private ICommand addCommand;
        private ICommand historyCommand;
        private ICommand saveCommand;
        private ICommand cancelCommand;
        private ICommand editCommand;


        [ImportingConstructor]
        public RgBuchDetailViewModel(IRgBuchDetailView view) : base(view)
        {
            isReadOnly = true;
        }


        public void SetFocus(FocusTarget target)
        {
            ViewCore.SetFocus(target);
        }


#region P R O P E R T I E S

    
        private IEnumerable mandanten;

        public IEnumerable Mandanten
        {
            
            get { return mandanten; }
            
            set {

                if (mandanten != value) {

                    mandanten = value;
                    RaisePropertyChanged("Mandanten");
                    RaisePropertyChanged("Buchungsjahre");
                    RaisePropertyChanged("CurrentMandant");
                    RaisePropertyChanged("CurrentJahr");
                }
            }
        }

        public IEnumerable Buchungsjahre
        {

            get {

                if (currentMandant == null) return null;
                return currentMandant.Buchungsjahre;
            }
        }

        private Mandant currentMandant;

        public Mandant CurrentMandant
        {
            
            get { return currentMandant; }
            
            set {

                if (currentMandant != value) {

                    currentMandant = value;
                    RaisePropertyChanged("CurrentMandant");
                    RaisePropertyChanged("Buchungsjahre");
                }
            }
        }

        private Buchungsjahr currentJahr;

        public Buchungsjahr CurrentJahr
        {
             
            get { return currentJahr; }
            
            set {

                if (currentJahr != value) {

                    currentJahr = value;
                    RaisePropertyChanged("CurrentJahr");
                }
            }
        }


        private bool switchIsEnabled; 

        /// <summary>
        /// beschreibt ob Mandant/Buchungsjahr umgeschaltet werden kann
        /// </summary>
        public bool SwitchIsEnabled 
        { 
            
            get { return switchIsEnabled; }
            set { 
            
                if (switchIsEnabled != value)
                {
                    switchIsEnabled = value;                  
                    RaisePropertyChanged("SwitchIsEnabled");
                }
            }
        }



        private bool isReadOnly;
       
        /// <summary>
        /// beschreibt ob Maske bearbeitet werden kann
        /// </summary>
        public bool IsReadOnly
        {
            
            get { return isReadOnly; }
            
            set {

                if (isReadOnly != value) {

                    isReadOnly = value;
                    RaisePropertyChanged("IsReadOnly");
                    RaisePropertyChanged("EditControlVisibility");
                    RaisePropertyChanged("ReadControlVisibility");
                    RaisePropertyChanged("Betrag");
                }
            }
        } 
        
        public String EditControlVisibility { get { return isReadOnly ? "Hidden": "Visible"; } }
        public String ReadControlVisibility { get { return !isReadOnly ? "Hidden": "Visible"; } }
                     

        public ICommand EditCommand
        {
            get { return editCommand; }
            set 
            {
                if (editCommand != value)
                {
                    editCommand = value;
                    RaisePropertyChanged("EditCommand");
                }
            }
        }              
       
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set 
            {
                if (saveCommand != value)
                {
                    saveCommand = value;
                    RaisePropertyChanged("SaveCommand");
                }
            }
        }              
       
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set 
            {
                if (cancelCommand != value)
                {
                    cancelCommand = value;
                    RaisePropertyChanged("CancelCommand");
                }
            }
        }              
       
        public ICommand AddCommand
        {
            get { return addCommand; }
            set 
            {
                if (addCommand != value)
                {
                    addCommand = value;
                    RaisePropertyChanged("AddCommand");
                }
            }
        }              
         
        public ICommand HistoryCommand
        {
            get { return historyCommand; }
            set 
            {
                if (historyCommand != value)
                {
                    historyCommand = value;
                    RaisePropertyChanged("HistoryCommand");
                }
            }
        }              
         
        public Buchung Current
        { 
            
            get { return current; }
            set { 
            
                if (current != value)
                {
                    current = value;
                    RaisePropertyChanged("Current");
                    RaisePropertyChanged("BelegNr");
                    RaisePropertyChanged("Eingang");
                    RaisePropertyChanged("Rkz");
                    RaisePropertyChanged("RkzName");
                    RaisePropertyChanged("Nummer");
                    RaisePropertyChanged("Datum");
                    RaisePropertyChanged("Betrag");
                    RaisePropertyChanged("Konto");
                    RaisePropertyChanged("KontoNr");
                    RaisePropertyChanged("KontoName");
                    RaisePropertyChanged("Bemerkungen");
                }
            }
        }
         
        public int? BelegNr
        { 
            
            get { 
            
                if (current == null) return null;
                return current.BelegNr;
            }
        }
         
        public DateTime? Eingang
        { 
            
            get { 
            
                if (current == null) return null;
                return current.Eingang;
            }

            set { 
            
                if (current.Eingang != value) {

                    current.Eingang = value;
                    RaisePropertyChanged("Eingang");
                }
            }
        }
         
        public Rkz Rkz
        { 
            
            get {

                if (current == null) return null;
                return current.Rkz;
            }
            
            set { 
            
                if (current.Rkz != value) {

                    current.Rkz = value;
                    RaisePropertyChanged("Rkz");
                    RaisePropertyChanged("RkzName");
                }
            }
        }
         
        public String RkzName
        { 
            
            get { 
            
                if (current == null || current.Rkz == null) return String.Empty;
                if (current.Rkz.IDRkz == 0) return "Bitte wählen Sie per Doppelklick das gewünschte RKZ/FBZ aus der RKZ/FBZ-Liste.";
                return current.Rkz.Name;
            }
        }
                  
        public String Nummer
        { 
            
            get {
                
                if (current == null) return null;
                return current.Nummer;
            }

            set { 
            
                if (current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }
                  
        public DateTime? Datum
        { 
            
            get {

                if (current == null) return null;
                return current.Datum;
            }

            set { 
            
                if (current.Datum != value) {

                    current.Datum = value;
                    RaisePropertyChanged("Datum");
                }
            }
        }
                  
        public String Betrag
        { 
            
            get {
                    
                if (current == null) return String.Empty;
                if (!IsReadOnly && current.Betrag == 0) return String.Empty;
                return current.Betrag.ToString("C");
            }

            set { 
            
                decimal pr;
                if (!Decimal.TryParse(value, out pr)) return;

                if (current.Betrag != pr) {

                    current.Betrag = pr;
                    RaisePropertyChanged("Betrag");
                }
            }
        }
                
        public Konto Konto
        { 
            
            get {

                if (current == null) return null;
                return current.Konto;
            }

            set { 
            
                if (current.Konto != value) {

                    current.Konto = value;
                    RaisePropertyChanged("Konto");
                    RaisePropertyChanged("KontoNr");
                    RaisePropertyChanged("KontoName");
                }
            }
        }
         
        public String KontoNr
        { 
            
            get { 
            
                if (current == null || current.Konto == null) return String.Empty;
                if (current.Konto.IDKonto == 0) return "Bitte wählen Sie per Doppelklick das gewünschte Konto aus der Debitoren-/Kreditorenliste.";
                return current.Konto.KontoNr.ToString(); 
            }
        }
         
        public String KontoName
        { 
            
            get { 
            
                if (current == null || current.Konto == null || current.Konto.IDKonto == 0) return String.Empty;
                return current.Konto.Name; 
            }
        }
                          
        public String Bemerkungen
        { 
            
            get {
                if (current == null) return null;
                return current.Bemerkungen;
                }
            set { 
            
                if (current.Bemerkungen != value)
                {
                    current.Bemerkungen = value;
                    RaisePropertyChanged("Bemerkungen");
                }
            }
        }


#endregion

    }
}
