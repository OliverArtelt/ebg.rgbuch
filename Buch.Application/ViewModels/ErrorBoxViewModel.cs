﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ErrorBoxViewModel : ViewModel<IErrorBoxView>
    {

        bool showDetails;
        ICommand closeCmd;
        ICommand detailCmd;
        Exception myException;
        String friendlyMsg;


        [ImportingConstructor]
        public ErrorBoxViewModel(IErrorBoxView view) : base(view)
        { 

            showDetails = false;
            closeCmd = new DelegateCommand(Close);
            detailCmd = new DelegateCommand(ShowDetail);
        }  

        public String FriendlyMessage { get { return friendlyMsg; } }
         
        public String ExceptionMessage { get { return (showDetails)? myException.Message: null; } }
                      
        public String StackTrace { get { return (showDetails)? myException.ToString(): null; } }
                   
        public ICommand CloseCommand { get { return closeCmd; } } 
                     
        public ICommand DetailCommand { get { return detailCmd; } } 
         
                
        /// <summary>
        /// Fehler in einer Dialogbox anzeigen
        /// </summary>
        /// <param name="ex"></param>
        public void ShowException(Exception ex, String friendlyMessage)
        {
         
            myException = ex;
            friendlyMsg = friendlyMessage;
            RaisePropertyChanged("FriendlyMessage");
            RaisePropertyChanged("ExceptionMessage");
            RaisePropertyChanged("StackTrace");
            
            ViewCore.ShowDialog();      
        }


        public void Close()
        {
            ViewCore.Close();
        }


        public void ShowDetail()
        {

            showDetails = !showDetails;

            RaisePropertyChanged("ExceptionMessage");
            RaisePropertyChanged("StackTrace");
        }
    }
}
