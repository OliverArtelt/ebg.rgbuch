﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class PeBuchViewModel : ViewModel<IPeBuchView>
    {
        
        [ImportingConstructor]
        public PeBuchViewModel(IPeBuchView view) : base(view)
        {
        }
       

#region P R O P E R T I E S
                  


#endregion

    }
}
