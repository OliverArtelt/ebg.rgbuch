﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Rechnungsbuch.Application.Views;
using System.Windows.Input;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class RgBuchViewModel : ViewModel<IRgBuchView>
    {
        
        private object listView;
        private object detailView;
        private object historyView;
        private object rkzView;
        private object kontoView;
       
                
        [ImportingConstructor]
        public RgBuchViewModel(IRgBuchView view) : base(view)
        {
        }


#region P R O P E R T I E S
        
        public object BuchListView
        {
            get { return listView; }
            set 
            {
                if (listView != value)
                {
                    listView = value;
                    RaisePropertyChanged("BuchListView");
                }
            }
        }              
        
        public object BuchDetailView
        {
            get { return detailView; }
            set 
            {
                if (detailView != value)
                {
                    detailView = value;
                    RaisePropertyChanged("BuchDetailView");
                }
            }
        } 
        
        public object RkzView
        {
            get { return rkzView; }
            set 
            {
                if (rkzView != value)
                {
                    rkzView = value;
                    RaisePropertyChanged("RkzView");
                }
            }
        } 
        
        public object BuchHistoryView
        {
            get { return historyView; }
            set 
            {
                if (historyView != value)
                {
                    historyView = value;
                    RaisePropertyChanged("BuchHistoryView");
                }
            }
        } 
        
        public object KontoView
        {
            get { return kontoView; }
            set 
            {
                if (kontoView != value)
                {
                    kontoView = value;
                    RaisePropertyChanged("KontoView");
                }
            }
        } 

#endregion

    }
}
