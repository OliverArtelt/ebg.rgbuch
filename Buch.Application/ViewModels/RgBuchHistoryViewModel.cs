﻿using System;
using System.Linq;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Entities;
using System.Windows.Input;
using System.Collections.Generic;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class RgBuchHistoryViewModel : ViewModel<IRgBuchHistoryView>
    {

        private Buchung buchung;


        [ImportingConstructor]
        public RgBuchHistoryViewModel(IRgBuchHistoryView view) : base(view)
        {

            CloseCommand = new DelegateCommand(() => ViewCore.Close());
        }


        public void ShowModal()
        {

            ViewCore.ShowDialog();   
        }


#region P R O P E R T I E S


        public ICommand CloseCommand { get; private set; }

        public List<BuchungHistory> Historie { get; private set; }


        public Buchung Buchung
        { 
            
            get { return buchung; }
            set { 
            
                if (buchung != value)
                {
                    
                    buchung = value;
                    Historie = buchung.HistorySet.OrderByDescending(p => p.Stempel).ToList();

                    RaisePropertyChanged("Buchung");
                    RaisePropertyChanged("Title");
                    RaisePropertyChanged("Historie");
                }
            }
        }


        public String Title
        {
            
            get {

                if (buchung == null) return "Änderungsverfolgung";
                return String.Format("Änderungsverfolgung für {0}", buchung.BelegNr);
            }
        }


#endregion

    }
}
