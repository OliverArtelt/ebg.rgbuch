﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;
using Rechnungsbuch.Application.ViewModels;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class MainViewModel : ViewModel<IMainView>
    {
    
        private object currentModuleView;
        private String currentModule;
        private String title;
        private ICommand exitCommand;

        private ICommand rgBuchCommand;
        private ICommand peBuchCommand;


        [ImportingConstructor]
        public MainViewModel(IMainView view) : base(view)
        {

            title = "EBG Rechnungsbuch";
        }


#region P R O P E R T I E S

        
        public String RgBuchColor { get { return CurrentModule == "RgBuch" ? "Gold": "PaleTurquoise"; } }
        public String PeBuchColor { get { return CurrentModule == "PeBuch" ? "Gold": "PaleTurquoise"; } }


        private bool testMode;

        public bool TestMode 
        { 
            get { return testMode; }
            set { 
            
                if (testMode != value)
                {
                    testMode = value;
                    RaisePropertyChanged("TestMode");
                }
            }
        }

        
        public String CurrentModule 
        { 
            get { return currentModule; }
            set { 
            
                if (currentModule != value)
                {
                    currentModule = value;
                    RaisePropertyChanged("CurrentModule");
                    RaisePropertyChanged("RgBuchColor");
                    RaisePropertyChanged("PeBuchColor");
                }
            }
        }


        public ICommand RgBuchCommand 
        { 
            get { return rgBuchCommand; }
            set { 
            
                if (rgBuchCommand != value)
                {
                    rgBuchCommand = value;
                    RaisePropertyChanged("RgBuchCommand");
                }
            }
        }


        public ICommand PeBuchCommand 
        { 
            get { return peBuchCommand; }
            set { 
            
                if (peBuchCommand != value)
                {
                    peBuchCommand = value;
                    RaisePropertyChanged("PeBuchCommand");
                }
            }
        }

        
        public ICommand ExitCommand 
        { 
            get { return exitCommand; }
            set { 
            
                if (exitCommand != value)
                {
                    exitCommand = value;
                    RaisePropertyChanged("ExitCommand");
                }
            }
        }

        
        public String Title
        {
           
            get { return title; }
            set {

                if (title != value) {

                    title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }
        

        public object CurrentModuleView
        {
            get { return currentModuleView; }
            set 
            {
                if (currentModuleView != value)
                {
                    currentModuleView = value;
                    RaisePropertyChanged("CurrentModuleView");
                }
            }
        }

#endregion


        public void Show()
        {
            ViewCore.Show();
        }
    }
}
