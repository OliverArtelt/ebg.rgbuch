﻿using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Application.ViewModels
{
    
    [Export]
    public class EmptyViewModel : ViewModel<IEmptyView>
    {

        private String message;

        
        [ImportingConstructor]
        public EmptyViewModel(IEmptyView view) : base(view)
        {
        }
       

#region P R O P E R T I E S
                  
           
        public String Message 
        { 
            
            get { return message; }
            set { 
            
                if (message != value)
                {
                    message = value;
                    RaisePropertyChanged("Message");
                }
            }
        }


#endregion

    }
}
