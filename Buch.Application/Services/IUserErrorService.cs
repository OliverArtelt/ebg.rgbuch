﻿using System;


namespace Rechnungsbuch.Application.Services
{
    
    /// <summary>
    /// Fehler, die dem Benutzer anzuzeigen sind
    /// </summary>
    public interface IUserErrorService
    {

        void ShowAndLogException(Exception ex);
        void ShowException(Exception ex);
        void LogException(Exception ex);
        /// <summary>
        /// Exception-Message in benutzerfreundlichen String wandeln
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        String ExplainException(Exception ex);
    }
}
