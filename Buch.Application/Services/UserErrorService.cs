﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Rechnungsbuch.Catalog;
using Rechnungsbuch.Error;
using Rechnungsbuch.Application.ViewModels;


namespace Rechnungsbuch.Application.Services
{
    
    /// <summary>
    /// MessageBox für Fehler die der Benutzer mitgeteilt bekommt
    /// </summary>
    [Export(typeof(IUserErrorService))]
    public class UserErrorService : IUserErrorService
    {

        private const String defaultMsg = "Ein interner Fehler ist aufgetreten. Bitte wenden Sie sich an den Support.";


        [Import(typeof(IPartProvider))]
        public Lazy<IPartProvider> Container { get; set; }
        [Import(typeof(ILogger))]
        public Lazy<ILogger> Logger { get; set; }
        [ImportMany(typeof(IUserFriendlyMessage))]
        public IEnumerable<IUserFriendlyMessage> Translators { get; set; }

        
        public void ShowAndLogException(System.Exception ex)
        {

            LogException(ex);
            ShowException(ex);  
        }
        
        public void ShowException(System.Exception ex)
        {

            Lazy<ErrorBoxViewModel> exp = null;
             
            try {
             
                exp = Container.Value.CreateLazy<ErrorBoxViewModel>();
                exp.Value.ShowException(ex, ExplainException(ex));
            }
            finally {

                Container.Value.Destroy(exp); 
            }
        }

        public void LogException(System.Exception ex)
        {
            
            try {

                Logger.Value.Log(ex);

            } catch {}
        }

        public String ExplainException(System.Exception ex)
        {
            
            if (ex is UserException) return ex.Message;
                        
            foreach (var entry in Translators) {

                String msg = entry.Explain(ex);
                if (!String.IsNullOrEmpty(msg)) return msg;
            }

            return defaultMsg;
        }
    }
}
