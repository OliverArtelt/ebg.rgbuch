﻿using System;
using System.Linq;
using System.ComponentModel.Composition;
using System.Collections.Generic;


namespace Rechnungsbuch.Application.Services
{
    
    [Export(typeof(IUserFriendlyMessage))]
    public class UserFriendlyMessage : IUserFriendlyMessage
    {

        public String Explain(Exception ex)
        {

            if (ex.Message == "An error occurred during local report processing.")
                return "Der Bericht kann nicht erstellt werden. Möglicherweise ist die Anwendung falsch installiert.";

            return null;
        }
    }
}
