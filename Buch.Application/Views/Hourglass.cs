﻿using System;
using System.Windows.Input;


namespace Rechnungsbuch.Application.Views
{
    
    public class Hourglass : IDisposable
    {
        
        private Cursor previousCursor;

        
        public Hourglass()
        {
            
            previousCursor = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
        }

        public void Dispose()
        {
            Mouse.OverrideCursor = previousCursor;
        }
    }
}
