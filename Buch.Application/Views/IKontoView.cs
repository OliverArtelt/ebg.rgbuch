﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IKontoView : IView
    {

        void DisplayItem(object o);
        void SetEditCursor();
    }
}
