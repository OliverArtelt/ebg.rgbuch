﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IRgBuchListView : IView
    {

        void DisplayItem(object o);
    }
}
