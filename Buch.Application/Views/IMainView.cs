﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IMainView : IView
    {
        void Show();
    }
}
