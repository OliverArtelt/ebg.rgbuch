﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IErrorBoxView : IView
    {

        bool? ShowDialog();
        void Close();
    }
}
