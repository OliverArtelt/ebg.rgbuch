﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IRgBuchDetailView : IView
    {
        void SetFocus(FocusTarget target);
    }
}
