﻿using System.Waf.Applications;


namespace Rechnungsbuch.Application.Views
{
    
    public interface IRgBuchHistoryView : IView
    {

        bool? ShowDialog();
        void Close();
    }
}
