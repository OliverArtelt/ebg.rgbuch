﻿using System.Windows;
using System.ComponentModel.Composition;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IMainView))]
    public partial class MainView : Window, IMainView
    {
        
        public MainView()
        {
            InitializeComponent();
        }
    }
}
