﻿using System.Windows.Controls;
using System.ComponentModel.Composition;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Application.ViewModels;
using System.Windows;
using System;
using System.Windows.Media;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IRkzView))]
    public partial class RkzView : UserControl, IRkzView
    {
        

        public RkzView()
        {
            InitializeComponent();
        }


        private void list_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            var dataCtx = DataContext as RkzViewModel;
            if (dataCtx != null && dataCtx.TakeCommand.CanExecute(null)) dataCtx.TakeCommand.Execute(null);
        }
    }
}
