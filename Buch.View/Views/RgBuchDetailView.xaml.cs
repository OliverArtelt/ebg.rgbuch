﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Views.Internal;
using System.Windows;
using System;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IRgBuchDetailView))]
    public partial class RgBuchDetailView : UserControl, IRgBuchDetailView
    {
        
        public RgBuchDetailView()
        {
            InitializeComponent();
        }

        public void SetFocus(FocusTarget target)
        {

            UIElement elm = null;

            if (target == FocusTarget.FirstCommand) elm = newButton;
            else if (target == FocusTarget.FirstInputControl) elm = datEingang;

            if (elm == null) return;

            System.Threading.ThreadPool.QueueUserWorkItem(
                   (a) =>
                        {
                            System.Threading.Thread.Sleep(100);
                            elm.Dispatcher.Invoke(
                            new Action(() =>
                            {
                                elm.Focus();
                            }));
                        }
                   );
        }
    }
}
