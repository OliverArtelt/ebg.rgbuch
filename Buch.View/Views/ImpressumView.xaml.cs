﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IImpressumView))]
    public partial class ImpressumView : UserControl, IImpressumView
    {
        public ImpressumView()
        {
            InitializeComponent();
        }
    }
}
