﻿using System.Windows.Controls;
using System.ComponentModel.Composition;
using Rechnungsbuch.Application.Views;
using Rechnungsbuch.Application.ViewModels;
using System.Windows;
using System;
using System.Windows.Input;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IKontoView))]
    public partial class KontoView : UserControl, IKontoView
    {
        
        public KontoView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Konto einfügen: zur angefügten Zeile scrollen und Schreibmarke setzen
        /// </summary>
        /// <param name="o">neues Konto</param>
        public void DisplayItem(object o)
        {
        
            list.SelectedItem = o; 
            //list.UpdateLayout();         
            //list.ScrollIntoView(list.SelectedItem, list.Columns[1]);
        }


        public void SetEditCursor()
        {

            System.Threading.ThreadPool.QueueUserWorkItem(
                   (a) =>
                        {
                            System.Threading.Thread.Sleep(100);
                            txtName.Dispatcher.Invoke(
                            new Action(() =>
                            {
                                txtName.Focus();
                                txtName.SelectAll();
                            }));
                        }
                   );
            //Keyboard.Focus(txtName); 
            //txtName.SelectAll();
        }

        /// <summary>
        /// Konto für Buchung wählen
        /// </summary>
        private void list_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            var dataCtx = DataContext as KontoViewModel;
            if (dataCtx != null && dataCtx.TakeCommand.CanExecute(null)) dataCtx.TakeCommand.Execute(null);
        }

        /// <summary>
        /// Search-Command anstoßen
        /// </summary>
        private void EnterKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key != System.Windows.Input.Key.Enter) return;
            var ctx = DataContext as KontoViewModel;

            if (ctx != null && ctx.SearchCommand.CanExecute(null)) ctx.SearchCommand.Execute(null);
        }
    }
}
