﻿using System.Windows.Controls;
using Rechnungsbuch.Application.Views;
using System.ComponentModel.Composition;


namespace Rechnungsbuch.Views
{
    
    [Export(typeof(IRgBuchView))]
    public partial class RgBuchView : UserControl, IRgBuchView
    {
        
        public RgBuchView()
        {
            InitializeComponent();
        }
    }
}
