﻿using System.ComponentModel.Composition;
using System.Windows;
using Rechnungsbuch.Application.ViewModels;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IRgBuchHistoryView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class RgBuchHistoryView : Window, IRgBuchHistoryView
    {
        
        public RgBuchHistoryView()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            
            if (e.Key == System.Windows.Input.Key.Escape) {

                var ctx = DataContext as RgBuchHistoryViewModel;
                if (ctx != null) ctx.CloseCommand.Execute(null);
            }
        }
    }
}
