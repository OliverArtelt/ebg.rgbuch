﻿using System;
using System.Globalization;
using System.Windows.Data;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Views.Internal
{
    
    /// <summary>
    /// Historie: Vorgangstyp (Insert,Update,Delete) farblich kennzeichnen
    /// </summary>
    [ValueConversion(typeof(String), typeof(String))]
    internal class ColoredDmlTypeConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            switch ((String)value) {

                case "I": return "PaleGreen";
                case "U": return "Khaki";
                case "D": return "Salmon";
            }

            return "White";
        }
 
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            throw new NotSupportedException("ColoredDmlTypeConverter.ConvertBack");
        }
    }
}
