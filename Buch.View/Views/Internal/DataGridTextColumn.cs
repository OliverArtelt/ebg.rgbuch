﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace Rechnungsbuch.Views.Internal
{
    
    /// <summary>
    /// Schreiben von Steuerzeichen in das Control unterdrücken
    /// </summary>
    public class CorrectedDataGridTextColumn : DataGridTextColumn
    {

        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs)
        {
	        
            TextBox textBox = editingElement as TextBox;
	        
            if (textBox != null) {

		        textBox.Focus();
		        string text = textBox.Text;
		        TextCompositionEventArgs textCompositionEventArgs = editingEventArgs as TextCompositionEventArgs;
		        
                if (textCompositionEventArgs != null) {

			        string text2 = (textCompositionEventArgs.Text == "\b")? String.Empty: textCompositionEventArgs.Text;
			        textBox.Text = text2;
			        textBox.Select(text2.Length, 0);
		        
                } else {

			        MouseButtonEventArgs mouseButtonEventArgs = editingEventArgs as MouseButtonEventArgs;
			    
                    if (mouseButtonEventArgs == null || !PlaceCaretOnTextBox(textBox, Mouse.GetPosition(textBox))) {
				        
                        textBox.SelectAll();
			        }
		        }

		        return text;
	        }

	        return null;
        }


        private static bool PlaceCaretOnTextBox(TextBox textBox, Point position)
        {
	        int characterIndexFromPoint = textBox.GetCharacterIndexFromPoint(position, false);
	        if (characterIndexFromPoint >= 0)
	        {
		        textBox.Select(characterIndexFromPoint, 0);
		        return true;
	        }
	        return false;
        }
    }
}
