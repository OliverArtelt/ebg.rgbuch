﻿using System;
using System.Globalization;
using System.Windows.Data;
using Rechnungsbuch.Entities;


namespace Rechnungsbuch.Views.Internal
{
    
    /// <summary>
    /// Kontotyp (Debitor/Kreditor) farblich kennzeichnen
    /// </summary>
    [ValueConversion(typeof(Kontotyp), typeof(String))]
    internal class ColoredKontoTypeConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            switch ((Kontotyp)value) {

                case Kontotyp.Debitor:  return "HoneyDew";
                case Kontotyp.Kreditor: return "Lavender";
                case Kontotyp.Sonstige: return "LightYellow";
            }

            return "Coral";
        }
 
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            throw new NotSupportedException("ColoredKontoTypeConverter.ConvertBack");
        }
    }
}
