﻿using System.Windows.Controls;
using Rechnungsbuch.Application.Views;
using System.ComponentModel.Composition;


namespace Rechnungsbuch.Views
{
    
    [Export(typeof(IPeBuchView))]
    public partial class PeBuchView : UserControl, IPeBuchView
    {
        
        public PeBuchView()
        {
            InitializeComponent();
        }
    }
}
