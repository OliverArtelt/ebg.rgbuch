﻿using System.Windows.Controls;
using Rechnungsbuch.Application.Views;
using System.ComponentModel.Composition;
using Rechnungsbuch.Application.ViewModels;
using System;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IRgBuchListView))]
    public partial class RgBuchListView : UserControl, IRgBuchListView
    {
        
        public RgBuchListView()
        {
            InitializeComponent();
        }

        public void DisplayItem(object o)
        {
                
            //list.ScrollIntoView(o);
        }

        /// <summary>
        /// Search-Command anstoßen
        /// </summary>
        private void EnterKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key != System.Windows.Input.Key.Enter) return;
            var ctx = DataContext as RgBuchListViewModel;

            if (ctx != null && ctx.SearchCommand.CanExecute(null)) ctx.SearchCommand.Execute(null);
        }
    }
}
