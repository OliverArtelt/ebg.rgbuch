﻿using System.Windows;
using System.ComponentModel.Composition;
using Rechnungsbuch.Application.Views;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IErrorBoxView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ErrorBoxView : Window, IErrorBoxView
    {
        
        public ErrorBoxView()
        {
            InitializeComponent();
        }
    }
}
