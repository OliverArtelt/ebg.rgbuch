﻿using System.Windows.Controls;
using Rechnungsbuch.Application.Views;
using System.ComponentModel.Composition;


namespace Rechnungsbuch.Views
{

    [Export(typeof(IEmptyView))]
    public partial class EmptyView : UserControl, IEmptyView
    {
        
        public EmptyView()
        {
            InitializeComponent();
        }
    }
}
