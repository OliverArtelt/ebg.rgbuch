﻿using System;


namespace Rechnungsbuch
{
    
    /// <summary>
    /// Exception in eine für den Benutzer verständliche Meldung wandeln
    /// </summary>
    public interface IUserFriendlyMessage
    {
        
        /// <summary>
        /// Benutzer mit einfachen Worten erklären was anliegt
        /// </summary>
        /// <param name="ex">zu übersetzende Exception</param>
        /// <returns>benutzerfreundliche Meldung</returns>
        string Explain(Exception ex);
    }
}
