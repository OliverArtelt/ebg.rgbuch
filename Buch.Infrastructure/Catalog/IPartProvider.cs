﻿using System;

namespace Rechnungsbuch.Catalog
{
    
    /// <summary>
    /// Composition-Container-Parts unittesttauglich/mocktauglich kapseln
    /// </summary>
    public interface IPartProvider
    {
        
        /// <summary>
        /// Erstelltes Objekt direkt geben
        /// </summary>
        /// <remarks>
        /// Objekt sollte nicht IDisposable implementieren, da der CompositionContainer dann eine Referenz darauf hält 
        /// und das Objekt vom GC nicht weggeräumt wird.
        /// </remarks>
        /// <returns>Part</returns>
        T Create<T>() where T : class;
        /// <summary>
        /// Erstelltes Objekt direkt geben
        /// </summary>
        /// <remarks>
        /// Objekt sollte nicht IDisposable implementieren, da der CompositionContainer dann eine Referenz darauf hält 
        /// und das Objekt vom GC nicht weggeräumt wird.
        /// </remarks>
        /// <returns>Part</returns>
        T Create<T>(String contractName) where T : class;
        /// <summary>
        /// Erstelltes Objekt als Lazy-Objekt geben
        /// </summary>
        /// <remarks>
        /// Dieses Objekt kann mit Destroy() aus dem CompositionContainer entfernt werden damit es der GC abräumen kann
        /// </remarks>
        /// <returns>Lazy-wrapped Part</returns>
        Lazy<T> CreateLazy<T>() where T : class;
        /// <summary>
        /// Erstelltes Objekt als Lazy-Objekt geben
        /// </summary>
        /// <remarks>
        /// Dieses Objekt kann mit Destroy() aus dem CompositionContainer entfernt werden damit es der GC abräumen kann
        /// </remarks>
        /// <returns>Lazy-wrapped Part</returns>
        Lazy<T> CreateLazy<T>(String contractName) where T : class;
        /// <summary>
        /// Erstelltes Lazy-Objekt aus dem CompositionContainer entfernen
        /// </summary>
        /// <remarks>
        /// Objekt wird ebenfalls vom CompositionContainer 'disposed' wenn es IDisposable implementiert
        /// </remarks>
        /// <returns>Lazy-wrapped Part</returns>
        void Destroy<T>(Lazy<T> export) where T : class;    
    }
}
