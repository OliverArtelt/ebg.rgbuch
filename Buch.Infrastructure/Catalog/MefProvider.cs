﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;


namespace Rechnungsbuch.Catalog
{
    
    [Export(typeof(IPartProvider))]
    public class MefProvider : IPartProvider
    {

        [Import(typeof(CompositionContainer))]
        public Lazy<CompositionContainer> Container { get; set; }

        
        public T Create<T>() where T: class
        {
            return Container.Value.GetExportedValue<T>();
        }


        public T Create<T>(String contractName) where T: class
        {
            return Container.Value.GetExportedValue<T>(contractName);
        }
    

        public Lazy<T> CreateLazy<T>() where T : class
        {
 	        return Container.Value.GetExport<T>();
        }


        public Lazy<T> CreateLazy<T>(string contractName) where T : class
        {
 	        return Container.Value.GetExport<T>(contractName);
        }


        public void Destroy<T>(Lazy<T> export) where T : class
        {
 	        Container.Value.ReleaseExport<T>(export);
        }
    }
}
