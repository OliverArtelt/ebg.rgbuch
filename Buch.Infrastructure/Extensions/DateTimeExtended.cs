﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Rechnungsbuch
{
    
    public static class DateTimeExtended
    {

        public static String AsHumanReadableText(this DateTime time)
        {

            if (time.Date == DateTime.Today) return String.Format("Heute, {0:t} Uhr", time);
            if (time.Date.AddDays(1) == DateTime.Today) return String.Format("Gestern, {0:t} Uhr", time);
            return time.ToString();
        }
    }
}
