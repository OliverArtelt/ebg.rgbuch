﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Rechnungsbuch.Error
{

    /// <summary>
    /// An den Benutzer direkt zu meldende Exception generieren
    /// </summary>
    public class UserException : ApplicationException
    {

        public UserException(string message) : base(message)
        {
        }

        public UserException(string message, Exception exception) : base(message, exception)
        {
        }
    }
}
