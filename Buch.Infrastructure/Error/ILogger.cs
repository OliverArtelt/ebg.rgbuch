﻿using System;


namespace Rechnungsbuch.Error
{
    
    public interface ILogger
    {

        void Log(Exception e);
        void Log(String info, bool asWarning = false);
    }
}
