﻿using System;
using System.Diagnostics;
using System.ComponentModel.Composition;
using System.Security.Permissions;


namespace Rechnungsbuch.Error
{

    /// <summary>
    /// Exception in das Ereignisprotokoll der Maschine schreiben
    /// </summary>
    [Export(typeof(ILogger))]
    public class Logger : ILogger
    {
    
        EventLog log;
        
        //public String ApplicationName { get; set; }

        [ImportingConstructor]
        public Logger([Import("Application.Name")]String applicationName)
        {

            log = new EventLog("Application"); 
            log.Source = applicationName;
        }
         
        /// <summary>
        /// Exception in das Ereignisprotokoll der Maschine schreiben
        /// </summary>
        /// <param name="e">Exception</param>
        public void Log(Exception e)
        {
        
            try {

                log.WriteEntry(e.ToString(), EventLogEntryType.Error); 
            }
            catch (Exception) {}
        }
         
        /// <summary>
        /// Hinweis-/Debugtext Ereignisprotokoll der Maschine schreiben
        /// </summary>
        /// <param name="info">Meldung</param>
        public void Log(String info, bool asWarning = false)
        {
        
            try {

                log.WriteEntry(info, asWarning? EventLogEntryType.Warning: EventLogEntryType.Information); 
            }
            catch (Exception) {}
        }
    }
}
