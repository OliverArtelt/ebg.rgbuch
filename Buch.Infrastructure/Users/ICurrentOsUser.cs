﻿using System;

namespace Rechnungsbuch.Users
{

    public interface ICurrentOsUser
    {
        
        string EMail { get; }
        string Name { get; }
        string SystemName { get; }
        string Vorname { get; }
    }
}
