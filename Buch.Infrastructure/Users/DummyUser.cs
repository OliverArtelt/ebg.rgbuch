﻿using System;
using System.ComponentModel.Composition;
using System.DirectoryServices;
using System.Security.Principal;
using System.Security.Authentication;


namespace Rechnungsbuch.Users
{
   
    [Export(typeof(ICurrentOsUser))]
    public class DummyUser : ICurrentOsUser
    {

        public String SystemName => "Systembenutzer";
        public String Name       => null;
        public String Vorname    => null;
        public String EMail      => null;
        public String UserName   => "Systembenutzer";
    }
}
