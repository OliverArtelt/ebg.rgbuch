﻿/* Migration DB Rechnungsbuch version 1  -> 2 */

USE [Rechnungsbuch2]
GO

/****** Object:  Table [dbo].[RebuchHistory]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[_qKto]'))
DROP VIEW [dbo].[_qKto]

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[qryChangesReFields]'))
DROP VIEW [dbo].[qryChangesReFields]

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tblRKZ_ChangeTracking]'))
DROP TRIGGER [dbo].[tblRKZ_ChangeTracking]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[qryChangesRebuch]'))
DROP VIEW [dbo].[qryChangesRebuch]

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[qryRebuch]'))
DROP VIEW [dbo].[qryRebuch]

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tblKonto_ChangeTracking]'))
DROP TRIGGER [dbo].[tblKonto_ChangeTracking]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[CalcBelegNr]'))
DROP TRIGGER [dbo].[CalcBelegNr]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tblRebuch_ChangeTracking]'))
DROP TRIGGER [dbo].[tblRebuch_ChangeTracking]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCounter]') AND type in (N'U'))
DROP TABLE [dbo].[tblCounter]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblHistory]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblKontenplan]') AND type in (N'U'))
DROP TABLE [dbo].[tblKontenplan]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblKontoKorr]') AND type in (N'U'))
DROP TABLE [dbo].[tblKontoKorr]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReKonto]') AND type in (N'U'))
DROP TABLE [dbo].[tblReKonto]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRePreis]') AND type in (N'U'))
DROP TABLE [dbo].[tblRePreis]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRechn]') AND type in (N'U'))
DROP TABLE [dbo].[tblRechn]
GO

ALTER TABLE [dbo].[tblKonto] ADD [updated] TIMESTAMP NOT NULL
GO

ALTER TABLE [dbo].[tblRebuch] ADD [updated] TIMESTAMP NOT NULL
GO

CREATE TABLE [dbo].[RebuchHistory](
	[KeyRebuch] [int] NOT NULL,
	[Stempel] [datetime] NOT NULL CONSTRAINT [DF_Table_1_stempel]  DEFAULT (getdate()),
	[Benutzer] [nvarchar](100) NOT NULL,
	[Typ] [char](1) NULL,
	[BelegNr] [int] NULL,
	[Eingang] [datetime] NULL,
	[KeyRkz] [int] NOT NULL,
	[KeyKonto] [int] NOT NULL,
	[Nummer] [nvarchar](160) NULL,
	[Datum] [datetime] NULL,
	[Betrag] [money] NOT NULL,
	[Bemerkungen] [ntext] NULL,
 CONSTRAINT [PK_RebuchHistory] PRIMARY KEY CLUSTERED 
(
	[KeyRebuch] ASC,
	[Stempel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndxRebuchHistory] ON [dbo].[RebuchHistory] 
(
	[KeyRebuch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/****** Object:  Table [dbo].[Users]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[iduser] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[vorname] [nvarchar](100) NOT NULL,
	[username] [nvarchar](100) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[iduser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Roles]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[idrole] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[idrole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Permissions]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[keyuser] [int] NOT NULL,
	[keyrole] [int] NOT NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[keyuser] ASC,
	[keyrole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Check [chkKontoName]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[tblKonto]  WITH CHECK ADD  CONSTRAINT [chkKontoName] CHECK  (([ktName]<>''))
GO
ALTER TABLE [dbo].[tblKonto] CHECK CONSTRAINT [chkKontoName]
GO
/****** Object:  Check [chkRkzName]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[tblRKZ]  WITH CHECK ADD  CONSTRAINT [chkRkzName] CHECK  (([rkzName]<>''))
GO
ALTER TABLE [dbo].[tblRKZ] CHECK CONSTRAINT [chkRkzName]
GO
/****** Object:  ForeignKey [fgnPermissionsRoles]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD  CONSTRAINT [fgnPermissionsRoles] FOREIGN KEY([keyrole])
REFERENCES [dbo].[Roles] ([idrole])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Permissions] CHECK CONSTRAINT [fgnPermissionsRoles]
GO
/****** Object:  ForeignKey [fgnPermissionsUsers]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD  CONSTRAINT [fgnPermissionsUsers] FOREIGN KEY([keyuser])
REFERENCES [dbo].[Users] ([iduser])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Permissions] CHECK CONSTRAINT [fgnPermissionsUsers]
GO
/****** Object:  ForeignKey [fgbBuchungHistoryKonto]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[RebuchHistory]  WITH CHECK ADD  CONSTRAINT [fgbBuchungHistoryKonto] FOREIGN KEY([KeyKonto])
REFERENCES [dbo].[tblKonto] ([IDKonto])
GO
ALTER TABLE [dbo].[RebuchHistory] CHECK CONSTRAINT [fgbBuchungHistoryKonto]
GO
/****** Object:  ForeignKey [fgnBuchungHistory]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[RebuchHistory]  WITH CHECK ADD  CONSTRAINT [fgnBuchungHistory] FOREIGN KEY([KeyRebuch])
REFERENCES [dbo].[tblRebuch] ([IDRebuch])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RebuchHistory] CHECK CONSTRAINT [fgnBuchungHistory]
GO

/****** Object:  ForeignKey [fgnBuchungHistoryRkz]    Script Date: 09/27/2011 15:01:53 ******/
ALTER TABLE [dbo].[RebuchHistory]  WITH CHECK ADD  CONSTRAINT [fgnBuchungHistoryRkz] FOREIGN KEY([KeyRkz])
REFERENCES [dbo].[tblRKZ] ([IDRKZ])
GO
ALTER TABLE [dbo].[RebuchHistory] CHECK CONSTRAINT [fgnBuchungHistoryRkz]
GO

/****** Object:  StoredProcedure [dbo].[InsertBuchung]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertBuchung]

	@BelegNr int = NULL, 
	@Eingang datetime = NULL, 
	@KeyRKZ int, 
	@KeyKonto int, 
	@Nummer nvarchar(160) = NULL, 
	@Datum datetime = NULL, 
	@Betrag money, 
	@Bemerkungen ntext = NULL

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @CurBuch INT;

    SELECT @CurBuch = MAX(reBelegNr) FROM tblRebuch --WHERE YEAR(reDatum) = YEAR(GETDATE()) 
	IF @CurBuch IS NULL
		 SET @BelegNr = 1
	ELSE 
		 SET @BelegNr = @CurBuch + 1	

	INSERT INTO tblRebuch (reBelegNr,reEingang,reKeyRKZ,reKeyKonto,reNummer,reDatum,reBetrag,reBemerkungen) VALUES (@BelegNr,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen)
	--INSERT INTO RebuchHistory (KeyRebuch,Benutzer,Typ,BelegNr,Eingang,KeyRKZ,KeyKonto,Nummer,Datum,Betrag,Bemerkungen) VALUES (SCOPE_IDENTITY(),SYSTEM_USER,'I',@BelegNr,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen)
	SELECT SCOPE_IDENTITY() AS BuchID WHERE @@ROWCOUNT > 0
END
GO

/****** Object:  StoredProcedure [dbo].[UpdateBuchung]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateBuchung]
	(
	@ID int,
	@BelegNr int = NULL, 
	@Eingang datetime = NULL, 
	@KeyRKZ int, 
	@KeyKonto int, 
	@Nummer nvarchar(160) = NULL, 
	@Datum datetime = NULL, 
	@Betrag money, 
	@Bemerkungen ntext = NULL,
	@ObjektVersion binary(8)
	)

AS
	SET NOCOUNT ON


	UPDATE tblRebuch SET reBelegNr=@BelegNr,reEingang=@Eingang,reKeyRKZ=@KeyRKZ,reKeyKonto=@KeyKonto,reNummer=@Nummer,reDatum=@Datum,reBetrag=@Betrag,reBemerkungen=@Bemerkungen WHERE IDRebuch=@ID AND updated=@ObjektVersion
	IF @@ROWCOUNT != 1 
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Optimistic concurrency exception', 4, 7)
		RETURN
	END
	
	INSERT INTO RebuchHistory (KeyRebuch,Benutzer,Typ,BelegNr,Eingang,KeyRkz,KeyKonto,Nummer,Datum,Betrag,Bemerkungen) VALUES (@ID,SYSTEM_USER,'U',@BelegNr,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen)

	RETURN
GO

/****** Object:  StoredProcedure [dbo].[DeleteBuchung]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteBuchung]
(
	@ID int,
	@ObjektVersion binary(8)
)

AS
	SET NOCOUNT ON

	DELETE FROM tblRebuch WHERE IDRebuch = @ID AND updated=@ObjektVersion
	IF @@ROWCOUNT != 1 
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Optimistic concurrency exception', 5, 5)
		RETURN
	END
	
	RETURN
GO

/****** Object:  StoredProcedure [dbo].[DeleteKonto]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteKonto]
	(
	@ID int,
	@ObjektVersion binary(8)
	)

AS
	SET NOCOUNT ON

	DELETE FROM tblKonto WHERE IDKonto=@ID AND updated=@ObjektVersion
	IF @@ROWCOUNT != 1 
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Optimistic concurrency exception', 4, 6)
		RETURN
	END

	RETURN
GO

/****** Object:  StoredProcedure [dbo].[UpdateKonto]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateKonto]
	(
	@ID int,
	@Konto int, 
	@Name nvarchar(160), 
	@Verwendung nvarchar(160) = NULL, 
	@ObjektVersion binary(8)
	)

AS
	SET NOCOUNT ON

	UPDATE tblKonto SET ktKonto=@Konto,ktName=@Name,ktVerwendung=@Verwendung WHERE IDKonto=@ID AND updated=@ObjektVersion
	/*
	IF @@ROWCOUNT != 1 
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Optimistic concurrency exception', 4, 8)
		RETURN
	END
	*/
	RETURN
GO

/****** Object:  StoredProcedure [dbo].[InsertKonto]    Script Date: 09/27/2011 15:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertKonto] 
	@Konto int, 
	@Name nvarchar(160), 
	@Verwendung nvarchar(160) = NULL 
AS
BEGIN

	SET NOCOUNT ON;
    DECLARE @CurKto INT;

	-- Sachkonten
	IF @Konto BETWEEN 60000 AND 69999
    BEGIN

		SELECT @CurKto = MAX(ktKonto) FROM tblKonto WHERE ktKonto BETWEEN 60000 AND 69999
		IF @CurKto = 69999
		BEGIN
			RAISERROR('Nummernkreis für Sachkonten erschöpft', 4, 2)
			ROLLBACK TRANSACTION
			RETURN
		END

		IF @CurKto IS NULL
			 SET @Konto = 60000
		ELSE 
			 SET @Konto = @CurKto + 1
	END

	-- Debitoren
	IF @Konto BETWEEN 13001 AND 19999
    BEGIN

		SELECT @CurKto = MAX(ktKonto) FROM tblKonto WHERE ktKonto BETWEEN 13001 AND 19999
		IF @CurKto = 19999
		BEGIN
			RAISERROR('Nummernkreis für Debitorenkonten erschöpft', 4, 3)
			ROLLBACK TRANSACTION
			RETURN
		END

		IF @CurKto IS NULL
			 SET @Konto = 13001
		ELSE 
			 SET @Konto = @CurKto + 1
	END

	-- Kreditoren
	IF (@Konto BETWEEN 74001 AND 79999) OR (@Konto BETWEEN 84000 AND 89999)
    BEGIN

		SELECT @CurKto = MAX(ktKonto) FROM tblKonto WHERE (ktKonto BETWEEN 74001 AND 79999 OR ktKonto BETWEEN 84000 AND 89999)
		IF @CurKto = 89999
		BEGIN
			RAISERROR('Nummernkreis für Kreditorenkonten erschöpft', 4, 4)
			ROLLBACK TRANSACTION
			RETURN
		END

		IF @CurKto IS NULL
			 SET @Konto = 74000
		ELSE IF @CurKto = 79999
			 SET @Konto = 84000
		ELSE 
			 SET @Konto = @CurKto + 1
	END

	INSERT INTO tblKonto (ktKonto,ktName,ktVerwendung) VALUES (@Konto,@Name,@Verwendung)
	SELECT SCOPE_IDENTITY() AS KontoID WHERE @@ROWCOUNT > 0
END
GO

-- Platzhalter
SET IDENTITY_INSERT tblKonto ON;
INSERT INTO tblKonto (IDKonto, ktKonto, ktName, ktVerwendung) VALUES (0, 0, 'Platzhalter für neue Buchung', NULL);
SET IDENTITY_INSERT tblKonto OFF;
SET IDENTITY_INSERT tblRKZ ON;
INSERT INTO tblRKZ (IDRKZ, rkzName) VALUES (0, 'Platzhalter für neue Buchung');
SET IDENTITY_INSERT tblRKZ OFF;
GO

-- Rechteverwaltung
SET IDENTITY_INSERT [dbo].[Roles] ON;
INSERT INTO [dbo].[Roles]([idrole],[name]) VALUES (1,'Rechnungsbuch.Schreiben')
INSERT INTO [dbo].[Roles]([idrole],[name]) VALUES (2,'Posteingangsbuch.Zugriff')
SET IDENTITY_INSERT [dbo].[Roles] OFF;
GO

SET IDENTITY_INSERT [dbo].[Users] ON;
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (1,'Wöhlert','Monika',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (2,'Boraschke','Grit',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (3,'Thomas','Christine',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (4,'Mainka','Sabine',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (5,'Jaab','Gabriele',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (7,'Wunder','Margitta',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (8,'Wagner','Susann',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (9,'Ölze','Karla',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (10,'Röwer','Manuela',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (11,'Karpe','Steffi',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (12,'Artelt','Oliver',NULL)
INSERT INTO [dbo].[Users] ([iduser],[name],[vorname],[username]) VALUES (13,'Voigt','Steffen',NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF;
GO

INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (1,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (2,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (2,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (3,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (3,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (4,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (4,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (5,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (5,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (7,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (7,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (8,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (9,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (10,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (11,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (12,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (12,2)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (13,1)
INSERT INTO [dbo].[Permissions]([keyuser],[keyrole]) VALUES (13,2)
GO
