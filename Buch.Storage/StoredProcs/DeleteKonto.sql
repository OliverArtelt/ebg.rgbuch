﻿/****** Objekt:  StoredProcedure [dbo].[DeleteKonto]    Skriptdatum: 09/27/2011 11:34:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DeleteKonto]
	(
	@ID int,
	@ObjektVersion binary(8)
	)

AS
	SET NOCOUNT ON

	DELETE FROM tblKonto WHERE IDKonto=@ID --AND updated=@ObjektVersion
	RETURN

