﻿/****** Objekt:  StoredProcedure [dbo].[UpdateKonto]    Skriptdatum: 09/27/2011 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UpdateKonto]
	(
	@ID int,
	@Konto int, 
	@Name nvarchar(160), 
	@Verwendung nvarchar(160) = NULL, 
	@ObjektVersion binary(8),
	@Mandant int,
	@Typ int
	)

AS
	SET NOCOUNT ON

	UPDATE tblKonto SET ktKonto=@Konto,ktName=@Name,ktVerwendung=@Verwendung WHERE IDKonto=@ID --AND updated=@ObjektVersion
	RETURN