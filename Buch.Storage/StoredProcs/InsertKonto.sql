﻿/****** Objekt:  StoredProcedure [dbo].[InsertKonto]    Skriptdatum: 09/27/2011 11:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[InsertKonto] 
	@Konto int, 
	@Name nvarchar(160), 
	@Verwendung nvarchar(160) = NULL,
	@Mandant int,
	@Typ int 
AS
BEGIN

	SET NOCOUNT ON;
    DECLARE @CurKto INT;

	SELECT @CurKto = MIN(new) 
	FROM (
	SELECT von,bis,CASE WHEN cur=0 THEN von ELSE cur+1 END AS new
	FROM 
	( SELECT von,bis,(SELECT COALESCE(MAX(ktKonto), 0)
					 FROM tblKonto k
					 WHERE k.KeyMandant=@Mandant AND ktKonto BETWEEN von AND bis) AS cur
	  FROM Kontenklasse
	  WHERE keymandant=@Mandant AND keytyp=@Typ AND standard=1
	) v
	WHERE cur <> bis
	) w

	IF @CurKto IS NULL
	BEGIN
		RAISERROR('Kein freies Konto in dieser Klasse gefunden.', 16, 1)
		RETURN
	END

	INSERT INTO tblKonto (ktKonto,ktName,ktVerwendung,KeyMandant,KeyTyp) VALUES (@CurKto,@Name,@Verwendung,@Mandant,@Typ)
	SELECT SCOPE_IDENTITY() AS KontoID WHERE @@ROWCOUNT > 0
END
