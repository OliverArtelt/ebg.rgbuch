﻿/****** Objekt:  StoredProcedure [dbo].[InsertBuchung]    Skriptdatum: 09/27/2011 11:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertBuchung]
	(
	@BelegNr int = NULL, 
	@Eingang datetime = NULL, 
	@KeyRKZ int, 
	@KeyKonto int, 
	@Nummer nvarchar(160) = NULL, 
	@Datum datetime = NULL, 
	@Betrag money, 
	@Bemerkungen ntext = NULL,
	@Buchungsjahr int
	)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @CurBuch INT;

    SELECT @CurBuch = CASE WHEN cur=0 THEN start ELSE cur+1 END 
	FROM (
	SELECT start,(SELECT COALESCE(MAX(reBelegNr), 0)
					FROM tblRebuch r
					WHERE r.KeyBuchungsjahr=@Buchungsjahr) AS cur
	FROM Buchungsjahr 
	WHERE idbuchungsjahr=@Buchungsjahr
	) v

	IF @CurBuch IS NULL
	BEGIN
		RAISERROR('Kann keine neue Buchungsnummer finden.', 16, 2)
		RETURN
	END

	INSERT INTO tblRebuch (reBelegNr,reEingang,reKeyRKZ,reKeyKonto,reNummer,reDatum,reBetrag,reBemerkungen,KeyBuchungsjahr) VALUES (@CurBuch,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen,@Buchungsjahr)
	--INSERT INTO RebuchHistory (KeyRebuch,Benutzer,Typ,BelegNr,Eingang,KeyRKZ,KeyKonto,Nummer,Datum,Betrag,Bemerkungen) VALUES (SCOPE_IDENTITY(),SYSTEM_USER,'I',@BelegNr,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen)
	SELECT SCOPE_IDENTITY() AS BuchID WHERE @@ROWCOUNT > 0
END