﻿/****** Objekt:  StoredProcedure [dbo].[ReleaseRebuchLock]    Skriptdatum: 10/10/2011 14:40:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ReleaseRebuchLock]

	@Id int

AS
BEGIN

	SET NOCOUNT ON
	DELETE FROM dbo.BuchLock WHERE id = @Id

	RETURN
END