﻿/****** Objekt:  StoredProcedure [dbo].[AcquireRebuchLock]    Skriptdatum: 10/10/2011 14:40:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AcquireRebuchLock]

	@Id int,
	@Owner nvarchar(120),
	@Station nvarchar(120) = NULL,
	@IsSuccessful bit OUTPUT,
	@OldOwner nvarchar(120) OUTPUT,
	@OldStation nvarchar(120) OUTPUT,
	@OldDate datetime OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	SELECT @OldOwner = [owner], @OldStation = [station], @OldDate = [created] FROM dbo.BuchLock WHERE id = @Id
	IF @@ROWCOUNT > 0
	BEGIN

		IF @OldDate > DATEADD(HOUR, -1, GETDATE())
		BEGIN
			SET @IsSuccessful = 0
			RETURN
		END

		DELETE FROM dbo.BuchLock WHERE id = @Id 	

	END

	SET @OldOwner = @Owner
	SET @OldStation = @Station
	SET @OldDate = GetDate()

	INSERT INTO dbo.BuchLock (id, created, [owner], station) VALUES (@Id, @OldDate, @OldOwner, @OldStation)
	SET @IsSuccessful = 1

END
