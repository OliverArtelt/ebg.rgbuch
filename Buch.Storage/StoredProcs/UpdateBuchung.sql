﻿/****** Objekt:  StoredProcedure [dbo].[UpdateBuchung]    Skriptdatum: 09/27/2011 11:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UpdateBuchung]
	(
	@ID int,
	@BelegNr int = NULL, 
	@Eingang datetime = NULL, 
	@KeyRKZ int, 
	@KeyKonto int, 
	@Nummer nvarchar(160) = NULL, 
	@Datum datetime = NULL, 
	@Betrag money, 
	@Bemerkungen ntext = NULL,
	@ObjektVersion binary(8),
	@Buchungsjahr int
	)

AS
	SET NOCOUNT ON

	UPDATE tblRebuch SET reBelegNr=@BelegNr,reEingang=@Eingang,reKeyRKZ=@KeyRKZ,reKeyKonto=@KeyKonto,reNummer=@Nummer,reDatum=@Datum,reBetrag=@Betrag,reBemerkungen=@Bemerkungen,KeyBuchungsjahr=@Buchungsjahr WHERE IDRebuch=@ID --AND updated=@ObjektVersion
	INSERT INTO RebuchHistory (KeyRebuch,Benutzer,Typ,BelegNr,Eingang,KeyRkz,KeyKonto,Nummer,Datum,Betrag,Bemerkungen) VALUES (@ID,SYSTEM_USER,'U',@BelegNr,@Eingang,@KeyRKZ,@KeyKonto,@Nummer,@Datum,@Betrag,@Bemerkungen)

	RETURN
