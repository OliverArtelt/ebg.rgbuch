﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using System.Windows;
using Rechnungsbuch.Application.Controllers;
using Rechnungsbuch.Application.Services;
using Rechnungsbuch.Error;
using Rechnungsbuch.Repositories;
using Rechnungsbuch.Views;


namespace Rechnungsbuch
{
  
    public partial class App : System.Windows.Application
    {
        
        private CompositionContainer container;
        private ApplicationController controller;


        protected override void OnStartup(StartupEventArgs e)
        {
            
            base.OnStartup(e);
            var cfg = new Configuration();

            try {

                AggregateCatalog catalog = new AggregateCatalog();
                catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(IUserFriendlyMessage).Assembly));
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(ApplicationController).Assembly));
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(MainView).Assembly));
                catalog.Catalogs.Add(new AssemblyCatalog(typeof(IBuchRepository).Assembly));

                container = new CompositionContainer(catalog);
                CompositionBatch batch = new CompositionBatch();     
                batch.AddExportedValue(container);
                container.Compose(batch);

#if (DEBUG)
               /*
                const String mefNspcPath = "Microsoft.ComponentModel.Composition.Diagnostics";
                const String mefAssmPath = @"C:\src\ebg\Rechnungsbuch\Library\MEF\Microsoft.ComponentModel.Composition.Diagnostics.dll";

                var mefAsm = Assembly.LoadFrom(mefAssmPath);
                var CIType = mefAsm.GetType(mefNspcPath + ".CompositionInfo");
                var CFType = mefAsm.GetType(mefNspcPath + ".CompositionInfoTextFormatter");
                var ciObj = Activator.CreateInstance(CIType, new object[] { catalog, container });
                //var sw = new StreamWriter("debug.log");
                CFType.InvokeMember("Write", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, 
                                    null, null, new object[] { ciObj, Console.Out });
               */
#endif

                controller = container.GetExportedValue<ApplicationController>();
                controller.Initialize();
                controller.Run();
            
            }
            catch (Exception x) { 
                
                try {

                    var svc = container.GetExportedValue<IUserErrorService>();
                    svc.ShowAndLogException(x);
                }
                catch {
                
                    var log = new Logger(cfg.ApplicationName);
                    log.Log(x);                
                }

#if (DEBUG)

                //Debugger.Break(); 
#endif
                
                System.Windows.Application.Current.Shutdown();
            }
        }
    
    
        protected override void OnExit(ExitEventArgs e)
        {

            if (container != null) container.Dispose();
            base.OnExit(e);
        }
    }
}
