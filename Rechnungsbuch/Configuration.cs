﻿using System;
using System.ComponentModel.Composition;


namespace Rechnungsbuch
{
   
    class Configuration
    {
       
        [Export("Application.Version")]
        public String ApplicationVersion        { get; private set; }
        
        [Export("Application.Name")]
        public String ApplicationName           { get; private set; }
        
        [Export("ConnectionString")]
        public string Connection                { get; private set; }
        
        [Export("Application.Test")]
        public bool ApplicationIstTestSystem    { get; private set; }


        public Configuration()
        {

            ApplicationName = "EBG Rechnungsbuch";
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            ApplicationVersion = String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
            ApplicationIstTestSystem = Properties.Settings.Default.IstTestApp;

            //Connection = Rechnungsbuch.Properties.Settings.Default.;
        }
    }
}
